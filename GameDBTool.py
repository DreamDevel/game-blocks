#!/usr/bin/env python

#Python Script To Download All Database From TheGameDB
# Before run do the following:
# - sudo apt-get install pip-python
# - sudo pip install requests
# - sudo pip install mysql-python

# TODO
# (DONE)Add Release Dates Into The SQL
# Add Developers To Database If Not Exist By Reading All Games Developers
# Add Publishers To Database If Not Exist By Reading All Games Publishers
# Add Try Catch Add Catch To Platforms/Games Download And File Read Write http://docs.python.org/2/tutorial/errors.html
#

# Notes
# 1. Release Dates Are North America
# 

import sys
import requests
import pickle
import MySQLdb
import time
from xml.dom import minidom
from subprocess import call

baseURI =  'http://thegamesdb.net/api/'

# Platform As Defined By http://thegamesdb.net/api/GetPlatformsList.php
class Platform:
    def __init__(self):
        self.id              = 0
        self.alias           = None
        self.platform        = None
        self.console         = None # Not Used , Unknown
        self.controller      = None # Not Used , Unknown
        self.overview        = None
        self.developer       = None
        self.manufacturer    = None
        self.cpu             = None
        self.memory          = None
        self.graphics        = None
        self.sound           = None
        self.display         = None
        self.media           = None
        self.maxControllers  = None
        self.rating          = None
        # Images Are Not Included

    def __str__(self):
        return ('ID           : ' + str(self.id)        + '\n' +
                'Platform     : ' + (self.platform       if (self.platform        is not None) else 'No Info') + '\n' +
                'Overview     : ' + (self.overview[0:30] if (self.overview        is not None) else 'No Info') + '\n' +
                'Developer    : ' + (self.developer      if (self.developer       is not None) else 'No Info') + '\n' +
                'Manufacturer : ' + (self.manufacturer   if (self.manufacturer    is not None) else 'No Info') + '\n' +
                'CPU          : ' + (self.cpu            if (self.cpu             is not None) else 'No Info') + '\n' +
                'Memory       : ' + (self.memory         if (self.memory          is not None) else 'No Info') + '\n' +
                'Graphics     : ' + (self.graphics       if (self.graphics        is not None) else 'No Info') + '\n' +
                'Sound        : ' + (self.sound[0:30]    if (self.sound           is not None) else 'No Info') + '\n' +
                'Display      : ' + (self.display        if (self.display         is not None) else 'No Info') + '\n' +
                'Media        : ' + (self.media          if (self.media           is not None) else 'No Info') + '\n' +
                'Max Contro.  : ' + (self.maxControllers if (self.maxControllers  is not None) else 'No Info') + '\n' +
                'Rating       : ' + (self.rating         if (self.rating          is not None) else 'No Info') + '\n' )
    def toInsertSQL(self):

        sqlQuery = ("INSERT INTO GameDB_Platforms(" + 'id' 
        + (",platform"       if (self.platform        is not None) else '')
        + (",overview"       if (self.overview        is not None) else '')
        + (",manufacturer"   if (self.manufacturer    is not None) else '')
        + (",cpu"            if (self.cpu             is not None) else '')
        + (",memory"         if (self.memory          is not None) else '')
        + (",graphics"       if (self.graphics        is not None) else '')
        + (",sound"          if (self.sound           is not None) else '')
        + (",display"        if (self.display         is not None) else '')
        + (",media"          if (self.media           is not None) else '')
        + (",maxcontrollers" if (self.maxControllers  is not None) else '')
        + (",rating"         if (self.rating          is not None) else '') + ") ")
        
        sqlQuery += ("VALUES(" + str(self.id) 
        + (",'" + self.platform       + "'"       if (self.platform        is not None) else '')
        + (",'" + self.overview       + "'"       if (self.overview        is not None) else '')
        + (",'" + self.manufacturer   + "'"       if (self.manufacturer    is not None) else '')
        + (",'" + self.cpu            + "'"       if (self.cpu             is not None) else '')
        + (",'" + self.memory         + "'"       if (self.memory          is not None) else '')
        + (",'" + self.graphics       + "'"       if (self.graphics        is not None) else '')
        + (",'" + self.sound          + "'"       if (self.sound           is not None) else '')
        + (",'" + self.display        + "'"       if (self.display         is not None) else '')
        + (",'" + self.media          + "'"       if (self.media           is not None) else '')
        + (",'" + self.maxControllers + "'"       if (self.maxControllers  is not None) else '')
        + ("," + self.rating                      if (self.rating          is not None) else '') + ");")
        
        return sqlQuery

    # To SQL To Use In GameBlocks
    def toInsertSQLGB(self):

        sqlQuery = ("INSERT INTO Platforms(" + 'GameDBID' 
        + (",Name"       if (self.platform        is not None) else '')
        + (",Manufacturer"   if (self.manufacturer    is not None) else '') + ") ")
        
        sqlQuery += ("VALUES(" + str(self.id) 
        + (",'" + self.platform       + "'"       if (self.platform        is not None) else '')
        + (",'" + self.manufacturer   + "'"       if (self.manufacturer    is not None) else '') + ");")
        
        return sqlQuery

class Game:
    def __init__(self):

        self.baseImageURI = 'http://thegamesdb.net/banners/'

        self.id           = 0
        self.gameTitle    = None
        self.platform     = None
        self.releaseDate  = None
        self.overview     = None
        self.publisher    = None
        self.developer    = None
        self.boxFront     = None
        self.boxBack      = None
        self.rating       = None # Not Used Yet (TODO)
        self.esrb         = None # Not Used Yet (TODO)
        self.genres       = None # Not Used Yet (TODO)
        # More Instances To Be Added (TODO)


    def __str__(self):

        coverFront = self.baseImageURI + (self.boxFront if (self.boxFront  is not None) else '')
        coverBack  = self.baseImageURI + (self.boxBack  if (self.boxBack   is not None) else '')
        
        return ('ID           : ' + str(self.id)        + '\n' +
                'Title        : ' + (self.gameTitle            if (self.gameTitle       is not None) else 'No Info') + '\n' +
                'Platform     : ' + (self.platform             if (self.platform        is not None) else 'No Info') + '\n' +
                'Overview     : ' + (self.overview[0:30]+'...' if (self.overview        is not None) else 'No Info') + '\n' +
                'Developer    : ' + (self.developer            if (self.developer       is not None) else 'No Info') + '\n' +
                'Publisher    : ' + (self.publisher            if (self.publisher       is not None) else 'No Info') + '\n' +
                'Released     : ' + (self.releaseDate          if (self.releaseDate     is not None) else 'No Info') + '\n' +
                'Cover        : ' + (coverFront                if (self.boxFront        is not None) else 'No Info') + '\n' +
                'Cover Back   : ' + (coverBack                 if (self.boxBack         is not None) else 'No Info') + '\n' )
    
    # To SQL To Use In GameBlocks
    def toInsertSQLGB(self):

        sqlQuery = ("INSERT INTO Games(" + 'GameDBID' 
        + (",Name"       if (self.platform        is not None) else '')
        + (",Manufacturer"   if (self.manufacturer    is not None) else '') + ") ")
        
        sqlQuery += ("VALUES(" + str(self.id) 
        + (",'" + self.platform       + "'"       if (self.platform        is not None) else '')
        + (",'" + self.manufacturer   + "'"       if (self.manufacturer    is not None) else '') + ");")
        
        return sqlQuery



class Request:
    
    def __init__(self):
        self.baseURI = 'http://thegamesdb.net/api/'

    def getPlatforms(self):
        platformsURI = self.baseURI + 'GetPlatformsList.php'
        platformURI  = self.baseURI + 'GetPlatform.php'

        xmlStr = requests.get(platformsURI).text
        xmlDom = minidom.parseString(xmlStr.encode('utf-8'))

        # Temp For Debug
        platformsList   = xmlDom.getElementsByTagName('Platform')
        platforms       = []
        platformCounter = 1
        
        for platformNode in platformsList :
            
            # For every platform create a platform object fill it and add it to platforms array
            platform       = Platform()
            if len(platformNode.getElementsByTagName('id')) > 0 :
                platform.id    = platformNode.getElementsByTagName('id')[0].firstChild.nodeValue
            #if len(platformNode.getElementsByTagName('name')) > 0 :
            #    platform.name  = platformNode.getElementsByTagName('name')[0].firstChild.nodeValue
            if len(platformNode.getElementsByTagName('alias')) > 0 :
                platform.alias = platformNode.getElementsByTagName('alias')[0].firstChild.nodeValue

            #New Request For Every Platform
            platformXMLStr = requests.get(platformURI,params={'id':platform.id}).text
            platformXMLDom = minidom.parseString(platformXMLStr.encode('utf-8'))

            platformDetailed = platformXMLDom.getElementsByTagName('Platform')[0]

            if len(platformDetailed.getElementsByTagName('Platform')) > 0 :
                platform.platform    = platformDetailed.getElementsByTagName('Platform')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('overview')) > 0 :
                platform.overview    = platformDetailed.getElementsByTagName('overview')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('developer')) > 0 :
                platform.developer   = platformDetailed.getElementsByTagName('developer')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('manufacturer')) > 0 :
                platform.manufacturer    = platformDetailed.getElementsByTagName('manufacturer')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('cpu')) > 0 :
                platform.cpu    = platformDetailed.getElementsByTagName('cpu')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('memory')) > 0 :
                platform.memory    = platformDetailed.getElementsByTagName('memory')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('graphics')) > 0 :
                platform.graphics    = platformDetailed.getElementsByTagName('graphics')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('sound')) > 0 :
                platform.sound    = platformDetailed.getElementsByTagName('sound')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('display')) > 0 :
                platform.display   = platformDetailed.getElementsByTagName('display')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('media')) > 0 :
                platform.media    = platformDetailed.getElementsByTagName('media')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('maxcontrollers')) > 0 :
                platform.maxControllers    = platformDetailed.getElementsByTagName('maxcontrollers')[0].firstChild.nodeValue.encode('utf-8')
            if len(platformDetailed.getElementsByTagName('Rating')) > 0 :
                platform.rating    = platformDetailed.getElementsByTagName('Rating')[0].firstChild.nodeValue.encode('utf-8')

            platform.toInsertSQL()
            platforms.append(platform)
            #sys.stdout.write("\r Downloaded Platform %d%% out of %d%%" %platformCounter %platformsList.length)
            sys.stdout.write('\rDownloaded Platform ' + str(platformCounter) + ' out of ' + str(platformsList.length))
            sys.stdout.flush()
            platformCounter += 1

        print('')
        return platforms

    def writePlatformsToSQL(self,path):
        platforms = self.getPlatforms()

        print('Clearing Previous Entries If Exist')
        myFile = open(path, 'w')
        myFile.write('')
        myFile.close()

        for platform in platforms:
            myFile = open(path, 'a')
            myFile.write(platform.toInsertSQLGB() + '\n\n')
            myFile.close()
            print('Platform ' + platform.platform + ' saved to file')

    def writeGamesToSQL(self,path,platformID):
        games = self.getGamesOfPlatform(platformID)

        print('Clearing Previous Entries If Exist')
        myFile = open(path, 'w')
        myFile.write('')
        myFile.close()

        for game in games:
            myFile = open(path, 'a')
            myFile.write(game.toInsertSQLGB() + '\n\n')
            myFile.close()
            print('Game ' + game.gameTitle + ' saved to file')

    def getGamesOfPlatform(self,platformID):
        gamesURI = self.baseURI + 'GetPlatformGames.php'
        gameURI  = self.baseURI + 'GetGame.php'


        xmlStr = requests.get(gamesURI,params={'platform':platformID}).text
        xmlDom = minidom.parseString(xmlStr.encode('utf-8'))

        # Temp For Debug
        gamesList   = xmlDom.getElementsByTagName('Game')
        gamesIDs    = []
        games       = []
        gamesCounter = 1
        

        for gameNode in gamesList :
            # For every platform create a platform object fill it and add it to platforms array
            #platform       = Platform()
            if len(gameNode.getElementsByTagName('id')) > 0 :
                gameID    = gameNode.getElementsByTagName('id')[0].firstChild.nodeValue
                gamesIDs.append(gameID)


        for gameID in gamesIDs :

            sys.stdout.write('\rDownloaded Game ' + str(gamesCounter) + ' Out Of ' + str(len(gamesIDs)) + ' - ID: ' + gameID)
            sys.stdout.flush()
            gamesCounter += 1

            gameXMLStr = requests.get(gameURI,params={'id':gameID}).text
            gameXMLDom = minidom.parseString(gameXMLStr.encode('utf-8'))

            gameDetailed = gameXMLDom.getElementsByTagName('Game')[0]
            game = Game()

            game.id = gameID

            if len(gameDetailed.getElementsByTagName('GameTitle')) > 0 :
                if gameDetailed.getElementsByTagName('GameTitle')[0].firstChild.nodeValue is not None:
                    game.gameTitle    = gameDetailed.getElementsByTagName('GameTitle')[0].firstChild.nodeValue.encode('utf-8')

            if len(gameDetailed.getElementsByTagName('Platform')) > 0  :
                if gameDetailed.getElementsByTagName('Platform')[0].firstChild.nodeValue is not None:
                    game.platform    = gameDetailed.getElementsByTagName('Platform')[0].firstChild.nodeValue.encode('utf-8')

            if len(gameDetailed.getElementsByTagName('Developer')) > 0 :
                if gameDetailed.getElementsByTagName('Developer')[0].firstChild.nodeValue is not None:
                    game.developer    = gameDetailed.getElementsByTagName('Developer')[0].firstChild.nodeValue.encode('utf-8')

            if len(gameDetailed.getElementsByTagName('Publisher')) > 0 :
                if gameDetailed.getElementsByTagName('Publisher')[0].firstChild.nodeValue is not None:
                    game.publisher    = gameDetailed.getElementsByTagName('Publisher')[0].firstChild.nodeValue.encode('utf-8')

            if len(gameDetailed.getElementsByTagName('ReleaseDate')) > 0 :
                if gameDetailed.getElementsByTagName('ReleaseDate')[0].firstChild.nodeValue is not None:
                    game.releaseDate  = gameDetailed.getElementsByTagName('ReleaseDate')[0].firstChild.nodeValue.encode('utf-8')
                    game.releaseDate  = time.strptime(game.releaseDate,"%m/%d/%Y")

            if len(gameDetailed.getElementsByTagName('Overview')) > 0 :
                if gameDetailed.getElementsByTagName('Overview')[0].firstChild.nodeValue is not None:
                    game.overview    = gameDetailed.getElementsByTagName('Overview')[0].firstChild.nodeValue.encode('utf-8')

            if len(gameDetailed.getElementsByTagName('boxart')) > 0 :
                for boxArt in gameDetailed.getElementsByTagName('boxart') :
                    if boxArt.getAttribute('side') == 'front' :
                        if  boxArt.firstChild.nodeValue is not None :
                            game.boxFront    = boxArt.firstChild.nodeValue.encode('utf-8')
                    if boxArt.getAttribute('side') == 'back'  :
                        if  boxArt.firstChild.nodeValue is not None :
                            game.boxBack     = boxArt.firstChild.nodeValue.encode('utf-8')
            
            games.append(game)

        print('')
        return games

class GameBlocks:
    def __init__(self):
        print('\r\033[38;5;208mInitializing Game Blocks\033[0;00m')
        self.request = Request()
        self.database = MySQLdb.connect(host="localhost", 
                                        user="root", 
                                        passwd="000000", 
                                        db="GameBlocks") 


    def writePlatformsToSQLFile(self,path,platforms):

        print('Clearing Previous Entries If Exist')
        myFile = open(path, 'w')
        myFile.write('')
        myFile.close()

        for platform in platforms:
            myFile = open(path, 'a')
            myFile.write(platform.toInsertSQLGB() + '\n\n')
            myFile.close()
            print('Platform ' + platform.platform + ' saved to file')

    #Platfroms Must Already Be In Database TODO: Re-Create Query Based On Attributes If Exist
    def writeGamesToSQLFile(self,path,games):
        print('Writing Games To SQL File')
        cursor = self.database.cursor()

        # TODO Create First All Developers
        devQuery = ''
        for game in games:
            if game.developer is not None :
                devQuery += "INSERT INTO Developers(Name) SELECT " + "'" + game.developer + "'" + " from Developers where not exists( select * from Developers  where Name = '"  + game.developer + "');" + '\n'
        
        # If devQuery is not empty execute it
        if len(devQuery) > 0 :
            myFile = open(path +'developers.sql', 'w')
            myFile.write(devQuery)
            myFile.close()
            print('\033[38;5;196mPlease Insert Developers Into The Database From The Created File And Press Enter\033[0;00m')
            raw_input()
        else :
            print("No Developers To Be Added")


        # TODO Create First All Publishers
        pubQuery = ''
        for game in games:
            if game.publisher is not None :
                pubQuery += "INSERT INTO Publishers(Name) SELECT " + "'" + game.publisher + "'" + " from Publishers where not exists( select * from Publishers  where Name = '"  + game.publisher + "');" + '\n'
        
        # If pubQuery is not empty execute it
        if len(pubQuery) > 0 :
            myFile = open(path +'publishers.sql', 'w')
            myFile.write(pubQuery)
            myFile.close()
            print('\033[38;5;196mPlease Insert Publishers Into The Database From The Created File And Press Enter\033[0;00m')
            raw_input()
        else :
            print("No Publishers To Be Added")


        print('Clearing Previous Entries If Exist')
        myFile = open(path + 'games.sql', 'w')
        myFile.write('')
        myFile.close()

        myFile = open(path + 'games.sql', 'a')
        for game in games:

            overview = ' '

            if game.gameTitle is None: 
                print('Game With ID :' + str(game.id) + ' has not any title. Will not be added to sql file')
                break

            if game.overview is None :
                overview = 'Unavailable'
            else :
                overview = game.overview

            cursor.execute('SELECT ID FROM Platforms WHERE Platforms.Name=' + "'" + game.platform + "'")
            platformID = cursor.fetchone()[0]

            if game.developer is not None :
                # Check if Developer Exist
                cursor.execute('SELECT ID FROM Developers WHERE Developers.Name=' + "'" + game.developer + "'")

                developerID = cursor.fetchone()
                if developerID is not None :
                    developerID = developerID[0]
                else :
                    print("Error Occured, Developer ID Couldn't Retrieved")

            if game.publisher is not None :
                # Check if Developer Exist
                cursor.execute('SELECT ID FROM Publishers WHERE Publishers.Name=' + "'" + game.publisher + "'")

                publisherID = cursor.fetchone()
                if publisherID is not None :
                    publisherID = publisherID[0]
                else :
                    print("Error Occured, Publisher ID Couldn't Retrieved")

            # Start Of The Query
            query = "INSERT INTO Games"

            # Start Of Query Fields
            query += "(Title,Description,PlatformID"
            
            if game.boxFront    is not None :
                query += ",CoverImageURL"

            if game.releaseDate is not None :
                query += ",ReleaseDateUSA"

            if game.developer   is not None :
                query += ",DeveloperID"

            if game.publisher   is not None :
                query += ",PublisherID"

            query += ") "

        
            # Start Of Query Values
            query += "Values("
            query += "'" + game.gameTitle.replace("'","''") + "',"
            query += "'" + overview.replace("'","''")  + "',"
            query += str(platformID)

            if game.boxFront is not None :
                query += ",'" + "http://thegamesdb.net/banners/" + game.boxFront + "'"

            if game.releaseDate is not None : 
                 query += ",'" + time.strftime("%Y-%m-%d",game.releaseDate) + "'"

            if game.developer   is not None :
                query += "," + str(developerID)

            if game.publisher   is not None :
                query += "," + str(publisherID)

            # End Of Query
            query += ");"
            
            myFile.write(query + '\n\n')
        myFile.close()
        self.database.close()

call(['clear'])
re = Request()
gb = GameBlocks()
print('\033[38;5;32mStage 1 - Creating Platform List\033[0;00m')
platforms = re.getPlatforms();
gb.writePlatformsToSQLFile('/home/fotini/Desktop/platforms.sql',platforms)
print('\033[38;5;196mPlease Insert Platforms Into The Database From The Created File And Press Enter To Continue\033[0;00m')
raw_input();

print('\033[38;5;32mStage 2 - Creating Games List\033[0;00m')
games     = re.getGamesOfPlatform(4920)
gb.writeGamesToSQLFile('/home/george/Desktop/SQL Files/',games)
print('\033[38;5;196mPlease Insert Games Into The Database From The Created File\033[0;00m')

writeToObject  = False
readFromObject = False
writeToSQLFile = False

# Create Platforms Insert SQL File
if not writeToObject and not readFromObject and writeToSQLFile :
    re.writePlatformsToSQL('/home/fotini/Desktop/platforms.sql')

# Write Platforms To File obj Or Read
if writeToObject :
    platforms = re.getPlatforms()
    myFile = open('/home/george/Desktop/platforms.obj','w')
    pickle.dump(platforms,myFile)
    myFile.close()
elif readFromObject :
    myFile = open('/home/george/Desktop/platforms.obj','r')
    platforms = pickle.load(myFile)
    myFile.close()
