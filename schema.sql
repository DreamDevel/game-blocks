SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_general_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_general_ci NOT NULL,
  `login` varchar(50) COLLATE utf8_general_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_general_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_general_ci NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_general_ci NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_general_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Site Roles`
--

CREATE TABLE IF NOT EXISTS `site_roles`( 
  `id` int NOT NULL AUTO_INCREMENT,
  `name`    varchar(128) NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------


--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_general_ci NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `role_id`    int ,
  `new_password_key` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_general_ci DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_general_ci NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY(`role_id`) REFERENCES site_roles(`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Platforms`
--

CREATE TABLE IF NOT EXISTS `Platforms`( 
  `ID`            int NOT NULL AUTO_INCREMENT,
  `GamesDBID`     int DEFAULT NULL,
  `Name`          varchar(128) NOT NULL,
  `Manufacturer`  varchar(128) DEFAULT NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Developers`
--

CREATE TABLE IF NOT EXISTS `Developers`( 
  `ID` int NOT NULL AUTO_INCREMENT,
  `Name`    varchar(128) NOT NULL,
  `Region`  varchar(128) ,
  PRIMARY KEY(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Publishers`
--

CREATE TABLE IF NOT EXISTS `Publishers`( 
  `ID` int NOT NULL AUTO_INCREMENT,
  `Name`    varchar(128) NOT NULL,
  `Region`  varchar(128) ,
  PRIMARY KEY(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Lists`
--

CREATE TABLE IF NOT EXISTS `Lists`( 
  `ID`      int NOT NULL AUTO_INCREMENT,
  `UserID`  int NOT NULL,
  `Name`    varchar(128) NOT NULL,
  `Public`  bool NOT NULL DEFAULT 1,
  PRIMARY KEY(id),
  FOREIGN KEY(UserID) REFERENCES users(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Games`
--

CREATE TABLE IF NOT EXISTS `Games`(
                        `ID`                  int NOT NULL AUTO_INCREMENT,
                        `GamesDBID`           int DEFAULT NULL,
                        `Title`               varchar(128) NOT NULL,
                        `Description`         text,
                        `PlatformID`          int   NOT NULL,
                        `DeveloperID`         int,
                        `PublisherID`         int,
                        `AddedTimeStamp`      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                        `CoverImageURL`       text ,
                        `ReleaseDateEUR`      date ,
                        `ReleaseDateUSA`      date ,
                        `ReleaseDateJPN`      date ,
                        `Online`              bool ,
                        `OnlineCoOp`          bool ,
                        `OnlinePlayers`       int  ,
                        `SplitScreen`         bool ,
                        `SplitScreenCoOp`     bool ,
                        `SplitScreenPlayers`  int  ,
                        `LanPlay`             bool ,
                        `LanPlayCoOp`         bool ,
                        `LanPlayPlayers`      int  ,
                        PRIMARY KEY(ID),
                        FOREIGN KEY(PlatformID) REFERENCES Platforms(id),
                        FOREIGN KEY(DeveloperID) REFERENCES Developers(id),
                        FOREIGN KEY(PublisherID) REFERENCES Publishers(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ListEntries`
--

CREATE TABLE IF NOT EXISTS `ListEntries`( 
  `GameID`  int NOT NULL,
  `ListID`  int NOT NULL,
  PRIMARY KEY(GameID,ListID),
  FOREIGN KEY(GameID) REFERENCES Games(id),
  FOREIGN KEY(ListID) REFERENCES Lists(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ListEntries`
--

CREATE TABLE IF NOT EXISTS `BeatenEntries`(
  `GameID`  int NOT NULL,
  `UserID`  int NOT NULL,
  PRIMARY KEY(GameID,UserID),
  FOREIGN KEY(GameID) REFERENCES Games(id),
  FOREIGN KEY(UserID) REFERENCES users(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `GDB_Platforms`
--

CREATE TABLE IF NOT EXISTS `GDB_Platforms`( 
  `ID`                  int  NOT NULL,
  `Platform`            TEXT NOT NULL,
  `Overview`            TEXT DEFAULT NULL,
  `Developer`           TEXT DEFAULT NULL,
  `Manufacturer`        TEXT DEFAULT NULL,
  `Cpu`                 TEXT DEFAULT NULL,
  `Memory`              TEXT DEFAULT NULL,
  `Graphics`            TEXT DEFAULT NULL,
  `Sound`               TEXT DEFAULT NULL,
  `Display`             TEXT DEFAULT NULL,
  `Media`               TEXT DEFAULT NULL,
  `MaxControllers`      TEXT DEFAULT NULL,
  `Linked`               BOOL NOT NULL DEFAULT 0,
-- Rating
-- FanArts
-- BoxArts
-- ConsoleArts
-- ControllerArts
  PRIMARY KEY(ID)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------


--
-- Table structure for table `GDB_Games`
--

CREATE TABLE IF NOT EXISTS `GDB_Games`( 
  `ID`                  int  NOT NULL,
  `GameTitle`           TEXT NOT NULL,
  `Platform`            TEXT NOT NULL,
  `PlatformID`          int  NOT NULL,
  `ReleaseDate`         TEXT DEFAULT NULL,
  `Overview`            TEXT DEFAULT NULL,
  `ESRB`                TEXT DEFAULT NULL,
  `Developer`           TEXT DEFAULT NULL,
  `Publisher`           TEXT DEFAULT NULL,
  `BoxArtFront`         TEXT DEFAULT NULL,
  `Linked`               BOOL NOT NULL DEFAULT 0,
-- Genres
-- Rating
-- FanArts
-- BoxArts
-- More fields to be added
  PRIMARY KEY(ID)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------


--
-- Table structure for table `GDB_GamesUpdate`
--

CREATE TABLE IF NOT EXISTS `GDB_GamesUpdate` (
  `ID`  int(11) NOT NULL,
  `PlatformID` int(11) NOT NULL,
  `TimeFound` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
);

-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `GameViews`( 
  `GameID`  int NOT NULL,
  `UserID`  int NOT NULL,
  `ViewTimeStamp`      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(GameID,UserID,ViewTimeStamp),
  FOREIGN KEY(GameID) REFERENCES Games(id),
  FOREIGN KEY(UserID) REFERENCES users(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

-- Create Site Roles And Give First User Admin Roles
INSERT INTO site_roles(name) VALUES('User');
INSERT INTO site_roles(name) VALUES('Administrator');


-- Debug Entries
/*INSERT INTO Platforms(Name,Manufacturer) VALUES('GameCube','Nintendo');
INSERT INTO Platforms(name,manufacturer) VALUES('Wii','Nintendo');
INSERT INTO Platforms(name,manufacturer) VALUES('Wii U','Nintendo');
INSERT INTO Platforms(name,manufacturer) VALUES('Playstation 3','Sony');
INSERT INTO Platforms(name,manufacturer) VALUES('Playstation 4','Sony');
INSERT INTO Platforms(name,manufacturer) VALUES('3DS','Nintendo');*/
INSERT INTO Publishers(name,region) VALUES('Nintendo','Japan');
INSERT INTO Publishers(name,region) VALUES('Sony Computer Entertainment','Japan');
INSERT INTO Developers(name,region) VALUES('Nintendo','Japan');
INSERT INTO Developers(name,region) VALUES('Sucker Punch Productions','USA');
INSERT INTO Developers(name,region) VALUES('Game Freak','Japan');

/*
-- Wii Games
INSERT INTO Games(title,description,platformid,developerid,publisherid,CoverImageURL) 
VALUES('Super Mario Galaxy',
  'No Description Available', 
  2, 1, 1,'http://www.ryanike.com/wp-content/uploads/2011/09/galaxy-1-cover1.jpg');

-- Wii U Games DEBUG
INSERT INTO Games(title,description,platformid,developerid,publisherid,CoverImageURL) 
VALUES('Legend Of Zelda: The Wind Waker',
  'The game is set on a group of islands in a vast sea—a first for the series. The player controls Link, the protagonist of the Zelda series. He struggles against his nemesis, Ganondorf, for control of a sacred relic known as the Triforce. Link spends a large portion of the game sailing, traveling between islands, and traversing dungeons and temples to gain the power necessary to defeat Ganondorf. He also spends time trying to find his little sister Aryll.', 
  3, 1, 1,'http://image.gamespotcdn.net/gamespot/images/box/1/1/0/701110_301775_front.jpg');

INSERT INTO Games(title,description,platformid,developerid,publisherid,CoverImageURL) 
VALUES('Super Mario 3D World',
  'Whilst exploring the Mushroom Kingdom, Mario, Luigi, Princess Peach, and Toad come across a strange clear pipe, which a curious green fairy-like creature, called a Sprixie, pops out of. Suddenly, Bowser appears and captures the fairy before escaping through the pipe. Thus, Mario and friends enter the pipe to pursue Bowser, and find themselves in the strange new Sprixie Kingdom as they search for the Sprixie, along with some of her other companions.', 
  3, 1, 1,'http://vgboxart.com/boxes/WiiU/53509-super-mario-3d-world.png');

-- Gamecube DEBUG
INSERT INTO Games(title,description,platformid,developerid,publisherid,CoverImageURL) 
VALUES('Luigi''s Mansion',
  'The game takes place in a haunted mansion when Luigi wins a contest that he never entered. He told his brother to meet him there to celebrate his victory. Luigi is searching for his brother Mario, who came to the mansion earlier, but went missing. To help Luigi on his quest, an old professor named Elvin Gadd or E. Gadd has equipped him with the "Poltergust 3000", a vacuum cleaner used for capturing ghosts, and a "Game Boy Horror", a device used for communicating with E. Gadd. He also uses it as a map and to examine ghosts.', 
  1, 1, 1,'http://www.mariowiki.com/images/thumb/5/5e/Lmbox.jpg/250px-Lmbox.jpg');


-- Playstation 4 DEBUG
INSERT INTO Games(title,description,platformid,developerid,publisherid,CoverImageURL) 
VALUES('inFAMOUS: Second Son',
  'Second Son is set seven years after the ending of Infamous 2, in which Cole MacGrath activated the Ray Field Inhibitor (RFI) to destroy a Conduit (superhumans named for their ability to "channel" powers) named John White, also known as the Beast. The explosion that resulted was originally thought to have killed all Conduits across the globe; however, those outside of the blast radius or with a natural resistance have survived. Fearing the Conduits abilities after the destruction of Empire City, the US eastern seaboard and the worldwide economy, the Department of Unified Protection (DUP) is formed. Now, major cities across the United States are heavily monitored for Conduit activity, whom the DUP have labeled collectively as "bio-terrorists"', 
  5, 2, 2,'http://upload.wikimedia.org/wikipedia/en/3/34/Infamous_second_son_boxart.jpg');


-- Nintendo 3DS DEBUG
INSERT INTO Games(title,description,platformid,developerid,publisherid,CoverImageURL) 
VALUES('Pokémon X',
  'Similar to previous installments, the two games follow the journey of a young Pokemon trainer (and their friends) as they travel through through the Kalos region, based on France, as they train Pokemon and thwart the schemes of the nefarious criminal organization Team Flare. X and Y introduced 69 new Pokemon and 28 new "Mega Evolutions", which some new features including a new fairy type, character customization, new battle and trainings mechanics, as well as completely rendered polygonal 3D graphics rather than sprites. Both titles are independent of each other, but feature largely the same plot, and while both can be played separately, trading Pokémon between both of the games is necessary in order to complete the games Pokédex.', 
  6, 3, 1,'http://thegamesdb.net/banners/boxart/original/front/18078-1.jpg');

INSERT INTO Games(title,description,platformid,developerid,publisherid,CoverImageURL) 
VALUES('Pokémon Y',
  'Similar to previous installments, the two games follow the journey of a young Pokemon trainer (and their friends) as they travel through through the Kalos region, based on France, as they train Pokemon and thwart the schemes of the nefarious criminal organization Team Flare. X and Y introduced 69 new Pokemon and 28 new "Mega Evolutions", which some new features including a new fairy type, character customization, new battle and trainings mechanics, as well as completely rendered polygonal 3D graphics rather than sprites. Both titles are independent of each other, but feature largely the same plot, and while both can be played separately, trading Pokémon between both of the games is necessary in order to complete the games Pokédex.', 
  6, 3, 1,'http://i1.wp.com/geekdad.com/wp-content/uploads/2013/10/pokemon-y.jpg');

INSERT INTO Games(title,description,platformid,developerid,publisherid,CoverImageURL) 
VALUES('Super Mario 3D Land',
  'No Description Available', 
  6, 3, 1,'http://www.padsandpanels.com/wp-content/uploads/2012/01/Super-Mario-3D-Land-Box.jpg');
  */