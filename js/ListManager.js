var ListManager = {
	
		baseUrl : '',

		initialize: function(baseUrl){

			var self = this;

			this.baseUrl = baseUrl;
		},

		addGame: function(gameID,listID){
			
			$.event.trigger('AddingGame');

			var url = this.baseUrl + "/gamelist/addGame/" + gameID + "/" + listID;
			$.ajax(url,{
				dataType: 'json',
				success: function(data){
			
					if(data['action'] == 'success'){
						$.event.trigger('GameAddedToList',[listID,gameID]);
					}
					else{
						console.error("Error Occured: " + data['errorType']);
					}
				}
			});
		},

		removeGame: function(gameID,listID){

			$.event.trigger('RemovingGame');

			var url = this.baseUrl + "/gamelist/removeGame/" + gameID + "/" + listID;
			$.ajax(url,{
				dataType: 'json',
				success: function(data){
			
					if(data['action'] == 'success'){
						$.event.trigger('GameRemovedFromList',[listID,gameID]);
					}
					else{
						console.error("Error Occured: " + data['errorType']);
					}
				}
			});
		},

		length: function(listID){
			var url = this.baseUrl + "/gamelist/length/" + listID;
			var length = 0;
			$.ajax(url,{
				async: false,
				dataType: 'json',
				timeout: 3000,
				success: function(data){
			
					if(data['action'] == 'success'){
						length = data['length'];
					}
					else{
						console.error("Error Occured: " + data['errorType']);
						length = null;
					}
				}
			});
			return length;
		}
};