var Notifier = {

	notifyListAction : function(numOfGames,action){
	
		var notifyTitle = "";
		if(action=="add")
			notifyTitle = 'Game Added Successfuly';
		else
			notifyTitle = 'Game Removed Successfuly';

		$.gritter.add({
					title: notifyTitle,
					text: 'Games in list: ' + numOfGames + ' Games',
					//image: 'http://a0.twimg.com/profile_images/59268975/jquery_avatar_bigger.png',
					sticky: false,
					time: '3000'
		});
	}
};