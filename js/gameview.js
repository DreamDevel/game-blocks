var gameView = {

	animation: false,
	game: null,

	initialize: function(animation,game,siteurl){

		var self = this;
		console.log("Intialize View");

		this.animation = animation;
		this.game = game;
		console.log(this.game.Title);

		// Initialize Objects
		self.toolBox.initialize(true);
		ListManager.initialize(siteurl);

		// Start Animation Or Display Page
		if(animation)
			initAnimated();
		else
			initStatic();

		// Calculate Cover Image Aspect Ratio And Change Size
		fixCoverImageSize();


		$(document).on('GameAddedToList',function(event,listID){
			var listLength = ListManager.length(listID);
			Notifier.notifyListAction(listLength,'add');
		});

		$(document).on('GameRemovedFromList',function(event,listID){
			var listLength = ListManager.length(listID);
			Notifier.notifyListAction(listLength,'remove');
		});


		function initStatic(){
			$('#loadIndicator').hide(0);
			$('#GameView').show(0);
			
		}

		function initAnimated(){
			// Show Indicator Until Elements Loaded
			$('#loadIndicator').show(0);

			$('#footer').hide(0);

			// Hide Game Blocks
			$('.head').css({height:'0px'});
				$('.head').find('.title').hide(0);
				$('.head').find('.toolbox').hide(0);
			$('.coverImage').css({left:'-500px',opacity:'0'});
			$('.generalInfo').css({right:'-1100px',opacity:'0'});
			$('.description').css({opacity:'0'});
			$('.multiplayer').css({opacity:'0'});
			$('.foot').css({height:'0px'});

			// When All Images are loaded start animation
			// TODO , Do something for broken image
			$(window).load(function(){
		

				$('#loadIndicator').hide(0);
				$('#footer').show(300);
				$('#GameView').show(0);
				$('.coverImage').animate({left:'10',opacity:'1'},700,function(){
					// Create A Small Crash Effect
					$(this).animate({left:''},150);
				});
				$('.generalInfo').animate({right:'10',opacity:'1'},700,function(){
					// Create A Small Crash Effect
					$(this).animate({right:''},150);
					// Display Other Elements
					$('.description').animate({opacity:'1'},700);
					$('.multiplayer').animate({opacity:'1'},700);
					$('.foot').animate({height:'50px'},700);
					$('.head').animate({height:'50px'},700,function(){
						// SHOW Title
						$('.head').find('.title').fadeIn(300);
						$('.head').find('.toolbox').fadeIn(300);
					});
				});
			});
		}

		function fixCoverImageSize(){

			// Calculate Image Based On Formula
			// original height / original width x new width = new height
			// http://math.stackexchange.com/questions/180804/how-to-get-the-aspect-ratio-of-an-image
			// Max Height = 350px
			var fixedImageWidth  = 0;
			var fixedImageHeight = 280;

			// Get on screen image
			var coverImage = $('.coverImage').find('img');
			// Create new offscreen image to test
			var theImage = new Image();
			theImage.src = coverImage.attr("src");

			// Get accurate measurements from that.
			var imageWidth = theImage.width;
			var imageHeight = theImage.height;

			fixedImageWidth = fixedImageHeight*imageWidth/imageHeight;

			coverImage.css({width:fixedImageWidth});
			coverImage.css({height:fixedImageHeight});
		}
	},

	toolBox : {
		animation : true,

		initialize : function(animation){

			var self = this;
			this.animation = animation;

			if(gameView.game.inCollection)
				$('.addGameBtn').hide(0);
			else
				$('.removeGameBtn').hide(0);

			$(document).on('AddingGame',function(){
				self.hideAdd();
			});

			$(document).on('RemovingGame',function(){
				self.hideRemove();
			});
		},
		hideAdd: function(){
			$('.addGameBtn').fadeOut(500,function(){
				$('.removeGameBtn').fadeIn(500);
			});
		},
		hideRemove: function(){
			$('.removeGameBtn').fadeOut(500,function(){
				$('.addGameBtn').fadeIn(500);
			});
		}

	}
};