<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once('gamesdblib/GDB_Request.php');
require_once('gamesdblibHelper/GDB_Local.php');
require_once('gamesdblibHelper/GDB_Link.php');

/**
 * GamesDB Library
 *
 * theGamesDB library for Code Igniter.
 *
 * @package		GDBLib
 * @author		George Sofianos (http://www.dreamdev.info)
 * @version		1.0.0
 * @license		Unknown Yet
 */
class GDBLib {

    public function __construct()
    {
    	$this->onlineDB = new GDB_Request();
    	$this->localDB  = new GDB_Local();
        $this->linkDB   = new GDB_Link();

    }

    // Returns an assosiative array with new games for each platform id
    public function checkForNewGames(){

        $platforms = $this->localDB->getAllPlatforms();
        $games       = [];
        $gamesUpdate = []; 
        $count = 0;
        foreach ($platforms as $platform){
            $id = $platform->getID();

            $gamesLocal  = $this->localDB->getGamesByPlatformID($id);
            $gamesOnline = $this->onlineDB->getGamesByPlatformID($id,true);

            $newGames = [];
            if( (!$gamesLocal && count($gamesOnline) > 0) || ( count($gamesOnline) > count($gamesLocal)) ) {

                // If we have no games localy for this platform, get all online games
                if(!$gamesLocal){

                    $newGames = $gamesOnline;
                }
                else{ // Create a list of games that we don't have

                    $newGames = [];
                    foreach($gamesOnline as $onlineGame){
                        $found = false;
                        foreach($gamesLocal as $localGame){

                            if($onlineGame->getID() == $localGame->getID())
                                $found = true;
                        }

                        // If we couldn't find game localy add it to new games
                        if(!$found)
                            $newGames[] = $onlineGame;
                    }
                }

                $gamesUpdate[(string)$id] = $newGames;
            }
        }

        // Add new games info in database
        foreach ($gamesUpdate as $platformID => $gameList){

            for($i=0; $i<count($gameList); $i++){

                // Check if this game is alreay in database update before inserting
                if(!$this->localDB->newGameExists($gameList[$i]->getID()))
                    $this->localDB->addNewGame($gameList[$i]->getID(),$platformID);
            }
        }        
    }

    // Returns an array of minimal platform objects(id,platform) , Null on error
    public function checkForPlatformUpdates(){

    	$platforms = $this->onlineDB->getPlatforms(true);

    	if(is_null($platforms))
    		return Null;

    	$platformsObj = [];
    	foreach ($platforms as $platform) {

    		// If platform doesn't exist add it
    		if(! $this->localDB->platformExists( array("id" => $platform->getID()) ) ) {

    			$platformsObj[] = $platform;
    		}
    	}

    	return $platformsObj;

    }

    // TODO Impement Full Update
    public function updateGames($fullUpdate=false){

        if(!$fullUpdate){

            // Get The List Of New Games
            $newGamesList = $this->localDB->getNewGames();

            foreach($newGamesList as $game){

                $gameObj = $this->onlineDB->getGameByID($game->ID);

                $this->localDB->addGame(
                    $gameObj->getID(),
                    $gameObj->getGameTitle(),
                    $gameObj->getPlatform(),
                    $gameObj->getPlatformID(),
                    $gameObj->getReleaseDate(),
                    $gameObj->getOverview(),
                    $gameObj->getESRB(),
                    $gameObj->getDeveloper(),
                    $gameObj->getPublisher(),
                    (isset($gameObj->getBoxArts()['front']) ? $gameObj->getBoxArts()['front'] : null)
                    );
               
               $this->localDB->removeNewGame($game->ID);

               // TODO Check if this game exists in main database,if not create a link

            }
        }
    }

    // TODO Drop All platforms in fullUpdate
    // Returns the number of platforms
    // Returns Null on error
    public function updatePlatforms($fullUpdate=false){

    	$platforms = Null;

    	// Get all platforms in full update or just the ones that are missing
    	if($fullUpdate){
    		$platforms = $this->onlineDB->getPlatforms();

    		if($platforms == Null)
    			return Null;
    	}
    	else {

    		$platforms = $this->checkForPlatformUpdates();
    		if(is_null($platforms))
    			return Null;

    		$platformsTemp = [];
    		foreach($platforms as $platform) {
    			$platform = $this->onlineDB->getPlatformByID($platform->getID());
    			if(is_null($platform))
    				return Null;
    			else
    				$platformsTemp[] = $platform;
    		}

    		$platforms = $platformsTemp;
    	}
    	

    	$counter = 0;

    	foreach ($platforms as $platform) {

    		// TODO if fullupdate Delete All Platforms

    		// If platform doesn't exist add it
    		if(! $this->localDB->platformExists( array("id" => $platform->getID()) ) ) {

    			$this->localDB->addPlatform(
    				$platform->getID(),
    				$platform->getPlatform(),
    				$platform->getOverview(),
    				$platform->getDeveloper(),
    				$platform->getManufacturer(),
    				$platform->getCpu(),
    				$platform->getMemory(),
    				$platform->getGraphics(),
    				$platform->getSound(),
    				$platform->getDisplay(),
    				$platform->getMedia(),
    				$platform->getMaxControllers()
    				);

    			$counter++;
    		}
    	}

    	return $counter;
    }

    public $onlineDB;
    public $localDB;

    
}

?>