<?php

require_once(APPPATH.'libraries/gamesdblib/GDB_Platform.php');
require_once(APPPATH.'libraries/gamesdblib/GDB_Game.php');

	class GDB_Link {

		function __construct()
		{
			$this->ci =& get_instance();
			$this->ci->load->database();
			$this->ci->load->model('GamesDB/platform_model','gdbPlatformModel');
			$this->ci->load->model('GamesDB/gdgame_model','gdbGameModel');
			$this->ci->load->model('GamesDB/gdgamesupdate_model','gdbGamesUpdate');

			$this->ci->load->model('game_model','GBGameModel');
			$this->ci->load->model('platforms_model','platformsModel');

		} 

		/* ---------------- Platforms -------------- */

		// Link All Unlinked Platforms
		public function linkPlatforms(){

			$newPlatforms = $this->getUnlinkedPlatforms();

			foreach($newPlatforms as $platform){

				$this->ci->platformsModel->add($platform->Platform,$platform->ID);

				$this->ci->gdbPlatformModel->setLinked($platform->ID,True);
			}


		}

		public function getUnlinkedPlatforms(){

			$unlinkedPlatforms = $this->ci->gdbPlatformModel->getAll(array("Linked"=>0));
			return $unlinkedPlatforms;

		}

		/* ---------------- Games -------------- */
		public function linkGames(){

			$newGames = $this->getUnlinkedGames();
			
			foreach($newGames as $game){

				// Check if platform exist and get id
				$GBPlatform = $this->ci->platformsModel->get(array('GamesDBID'=>$game->PlatformID));

				$this->ci->GBGameModel->add(array(
					'Title' => $game->GameTitle,
					'PlatformID' => $GBPlatform->ID,
					'GamesDBID' => $game->ID ));

				$this->ci->gdbGameModel->setLinked($game->ID,True);
			}

		}


		public function getUnlinkedGames(){

			$unlinkedGames = $this->ci->gdbGameModel->getAll(array("Linked"=>0));
			return $unlinkedGames;

		}
	}

?>