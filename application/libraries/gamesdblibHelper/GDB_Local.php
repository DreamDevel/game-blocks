<?php

require_once(APPPATH.'libraries/gamesdblib/GDB_Platform.php');
require_once(APPPATH.'libraries/gamesdblib/GDB_Game.php');

	class GDB_Local {

		function __construct()
		{
			$this->ci =& get_instance();
			$this->ci->load->database();
			$this->ci->load->model('GamesDB/platform_model','platformModel');
			$this->ci->load->model('GamesDB/gdgame_model','gameModel');
			$this->ci->load->model('GamesDB/gdgamesupdate_model','gamesUpdate');

		} 


		/* ---------------------------------------- Platform ------------------------------------ */


		public function addPlatform($id,$platform,$overview,$developer,$manufacturer,$cpu,$memory,$graphics,$sound,$display,$media,$maxControllers){

			return $this->ci->platformModel->add($id,$platform,$overview,$developer,$manufacturer,$cpu,$memory,$graphics,$sound,$display,$media,$maxControllers);
		}

		// Returns false if couldn't get platforms
		public function getAllPlatforms(){

			$platforms = $this->ci->platformModel->getAll();

			if(is_null($platforms))
				return false;

			$platforms = $this->modelPlatformsToGDBObj($platforms);

			return $platforms;


		}

		public function platformExists($arg) {

			if(array_key_exists("id",$arg)) {
				return $this->ci->platformModel->existsByID($arg['id']);
			}
			elseif(array_key_exists("name",$arg)) {
				return $this->ci->platformModel->existsByName($arg['name']);
			}
		}

		/* ---------------------------------------- Game ------------------------------------ */


		public function addGame($id,$gameTitle,$platform,$platformID,$releaseDate,$overview,$esrb,$developer,$publisher,$boxArtFront){
			return $this->ci->gameModel->add($id,$gameTitle,$platform,$platformID,$releaseDate,$overview,$esrb,$developer,$publisher,$boxArtFront);
		}

		public function getAllGames(){

			$games = $this->ci->gameModel->getAll();

			if(is_null($games))
				return false;

			$games = $this->modelGamesToGDBObj($games);

			return $games;
		}

		public function getGamesByPlatformID($platformID){
			$games = $this->ci->gameModel->getByPlatformID($platformID);

			if(is_null($games))
				return false;

			$games = $this->modelGamesToGDBObj($games);

			return $games;
		}

		public function countGames($platformID=null){
			return $this->ci->gameModel->count($platformID);
		}

		/* ---------------------------------------- Games Update ------------------------------------ */

		public function addNewGame($id,$platformID){
			return $this->ci->gamesUpdate->add($id,$platformID);
		}

		public function removeNewGame($id){
			$this->ci->gamesUpdate->remove($id);
		}

		public function newGameExists($id){
			return $this->ci->gamesUpdate->exists($id);
		}

		public function getNewGames($platformID = null){

			return $this->ci->gamesUpdate->getAll();
			// else get only platform specific games
		}

		public function getNewGamesPlatforms(){
			$platformIDs = $this->ci->gamesUpdate->getPlatformsID();

			$platforms = [];
			foreach($platformIDs as $platformID){

				$platforms[] = $this->ci->platformModel->getByID($platformID);
			}

			return $this->modelPlatformsToGDBObj($platforms);
		}

		public function countNewGames($platformID = null){
			return $this->ci->gamesUpdate->countUpdates($platformID);
		}

		public function getlastGamesUpdateCheck(){
			return $this->ci->gamesUpdate->getLastUpdateCheck();
		}

		/* ---------------------------------------- Other  ------------------------------------ */

		private function modelPlatformsToGDBObj($platformsSQL){

			$platforms = [];
			foreach($platformsSQL as $platformSQL){
				$platform = new GDB_Platform();

				$platform->setID($platformSQL->ID);
				$platform->setPlatform($platformSQL->Platform);
				$platform->setOverview($platformSQL->Overview);
				$platform->setDeveloper($platformSQL->Developer);
				$platform->setManufacturer($platformSQL->Manufacturer);
				$platform->setCpu($platformSQL->Cpu);
				$platform->setMemory($platformSQL->Memory);
				$platform->setGraphics($platformSQL->Graphics);
				$platform->setSound($platformSQL->Sound);
				$platform->setDisplay($platformSQL->Display);
				$platform->setMedia($platformSQL->Media);
				$platform->setMaxControllers($platformSQL->MaxControllers);

				$platforms[] = $platform;
			}

			return $platforms;

		}

		private function modelGamesToGDBObj($gamesSQL){

			$games = [];
			foreach($gamesSQL as $gameSQL){
				$game = new GDB_Game();

				$game->setID($gameSQL->ID);
				$game->setGameTitle($gameSQL->GameTitle);
				$game->setPlatform($gameSQL->Platform);
				$game->setPlatformID($gameSQL->PlatformID);
				$game->setReleaseDate($gameSQL->ReleaseDate);
				$game->setOverview($gameSQL->Overview);
				$game->setDeveloper($gameSQL->Developer);
				$game->setPublisher($gameSQL->Publisher);
				$game->setESRB($gameSQL->ESRB);
				$game->setBoxArts(array($gameSQL->BoxArtFront));


				$games[] = $game;
			}

			return $games;

		}

	}


?>