<?php 

	class Gameviews_model extends CI_Model{

		function __construct()
        {
           parent::__construct();
        } 


        function add($gameID,$userID){
           
          $data = array(
         'gameID' => $gameID ,
         'userID' => $userID   		 
		     );

          $this->db->insert('GameViews', $data); 

        }

        function hasGameViewedByUser($userID,$gameID) {

          //check for entries in the last 24 hours 
          $query = $this->db->query("SELECT * FROM GameViews WHERE UserID = $userID AND GameID = $gameID AND UNIX_TIMESTAMP(ViewTimeStamp) >= UNIX_TIMESTAMP()-24*60*60");

          if($query->num_rows()==0)
            return 0; 
          else 
            return 1;          
        } 

        // Gets all games that have views
        function getAllGames(){

          $query = $this->db->query("SELECT DISTINCT GameID FROM GameViews");

          if($query->num_rows()==0)
            return NULL; 

          foreach ($query->result() as $gameEntry)
               $gamesID[] = $gameEntry->GameID; 
           
          return $gamesID;
        }

        function get($gameID,$limitStart,$limitStop=null){

          $limitStart = $limitStart->getTimestamp();

          if($limitStop)
             $limitStop  = $limitStop->getTimeStamp();

          $str = ($limitStop ? "AND UNIX_TIMESTAMP(ViewTimeStamp) >= $limitStart AND UNIX_TIMESTAMP(ViewTimeStamp) <= $limitStop" :
          "AND UNIX_TIMESTAMP(ViewTimeStamp) >= $limitStart  AND UNIX_TIMESTAMP(ViewTimeStamp) <= UNIX_TIMESTAMP()");

          $query = $this->db->query("SELECT * FROM GameViews WHERE GameID = $gameID $str");

          $gameViews = 0;

          if($query->num_rows()==0)
            return $gameViews; 

          //count game views 
          foreach ($query->result() as $gameEntry)
               $gameViews++;

          return $gameViews;
        }
	}

?>