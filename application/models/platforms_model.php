<?php 

   class Platforms_Model extends CI_Model{

   	   function __construct(){

   	   	 parent::__construct();
   	   }

       function add($name,$gamesDBID=null, $manufacturer=null){

          // Create Array Of Values

          $data = array();
          if($name)      $data['Name']         = $name;      else return false; 
          if($gamesDBID) $data['GamesDBID' ]   = $gamesDBID;
          if($manufacturer)    $data['Manufacturer'] = $manufacturer;

          $this->db->insert('Platforms',$data);
       }

       function get($options){

          $query = $this->db->get_where('Platforms',$options);

          if($query->num_rows() == 0)
            return false;
          else
            return $query->row();

       }

        /**
        * Method:getAllPlatforms
        * returns array with developer objects
        *
        */
   	   function getAllPlatforms(){

   	   	  $query = $this->db->get('Platforms');
   	   	  $platforms = $query->result();

   	   	  if($query->num_rows()==0)
             return NULL; 

           return $platforms; 
   	   }

       /**
       *
       * Checks if platform exists with provided options
       *
       */
       function exists($options){

           $this->db->select("");
           if( array_key_exists('GamesDBID',$options) ){
             $this->db->where('GamesDBID',$options['GamesDBID']);
           }

           $query = $this->db->get("Platforms");

           if($query->num_rows()==0)
             return false;
           else
             return true;

           // Todo Add Other Options
       }
   }

 ?>