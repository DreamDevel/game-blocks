<?php

   class Gamebase_Model extends CI_Model
   {

        function __construct()
        {
           parent::__construct();

           $this->gamesTable    = 'Games';
           $this->gamesGDBTable = 'GDB_Games';
        }


        /**
        * Method:getByID
        * returns game object
        *
        */
        public function getByID($id)
        {

          $this->load->model('GamesDB/gdgame_model','GDBGameModel');

          //get the game with the given id
          $query = $this->db->get_where($this->gamesTable,array('ID'=>$id));

          if($query->num_rows()==0)
            return false;

          $gameSQL = $query->row();
          $gameGDB = $this->GDBGameModel->getByID($gameSQL->GamesDBID);

          // Todo Trow Error if gameGDB does not exist;

          // Create New Game Object
          $game = new stdClass();

          $game->ID = $gameSQL->ID;

          $game->DateAdded = $gameSQL->AddedTimeStamp;

          // -------------- Title -------------- //
          // Title is always available in GB
          $game->Title = new stdClass();
          $game->Title->Value = $gameSQL->Title;
          $game->Title->DataProvider = 'GB';

          // -------------- Platform -------------- //
          // Platform is always available in GB
          $query = $this->db->get_where('Platforms',array('ID'=>$gameSQL->PlatformID));
          $game->Platform = new stdClass();
          $game->Platform->Value = $query->row();
          $game->Platform->DataProvider = 'GB';

          // -------------- Description -------------- //
          // If Exists in our database add it
          $game->Description = new stdClass();
          if($gameSQL->Description){

            $game->Description->DataProvider = 'GB';
            $game->Description->Value = $gameSQL->Description;

          }
          else {

            // Get it from GamesDB
            if($gameGDB->Overview){
              $game->Description->DataProvider = 'GDB';
              $game->Description->Value = $gameGDB->Overview;
            }
            else // Null means we couldn't find value
              $game->Description = Null;
          }

          // -------------- Developer -------------- //
          $game->Developer = new stdClass();
          if($gameSQL->DeveloperID){

            $query = $this->db->get_where('Developers',array('ID'=>$gameSQL->DeveloperID));
            $game->Description->Value = $query->row();
            $game->Description->DataProvider = 'GB';
          }
          else {


            if($gameGDB->Developer){
              $game->Developer->DataProvider = 'GDB';
              $game->Developer->Value = $gameGDB->Developer;
            }
            else
              $game->Developer = Null;
          }

          // -------------- Publisher -------------- //
          $game->Publisher = new stdClass();
          if($gameSQL->PublisherID){

            $query = $this->db->get_where('Publishers',array('ID'=>$gameSQL->PublisherID));
            $game->Publisher->Value = $query->row();
            $game->Publisher->DataProvider = 'GB';
          }
          else {

            if($gameGDB->Publisher){
              $game->Publisher->DataProvider = 'GDB';
              $game->Publisher->Value = $gameGDB->Publisher;
            }
            else
              $game->Publisher = Null;
          }

          // -------------- Cover Image -------------- //
          $game->CoverImage = new stdClass();
          if($gameSQL->CoverImageURL){

            $game->CoverImage->Value = $gameSQL->CoverImageURL;
            $game->CoverImage->DataProvider = 'GB';
          }
          else {

            if($gameGDB->BoxArtFront){
              $game->CoverImage->DataProvider = 'GDB';
              $game->CoverImage->Value = $gameGDB->BoxArtFront;
            }
            else
              $game->CoverImage = Null;
          }
          // -------------- Release Date -------------- //
          $game->ReleaseDate = new stdClass();
          $game->ReleaseDate->General = new stdClass();
          $game->ReleaseDate->General->DataProvider = 'GDB';
          $game->ReleaseDate->General->Value = new DateTime($gameGDB->ReleaseDate);

          $game->ReleaseDate->USA = Null;
          $game->ReleaseDate->EUR = Null;
          $game->ReleaseDate->JPN = Null;

          // TODO Add Added TimeStamp

          return $game;
        }

   }

?>
