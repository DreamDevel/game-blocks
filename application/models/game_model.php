<?php

   class Game_Model extends CI_Model
   {
       
        function __construct()
        {
           parent::__construct();

           $this->load->model('gamebase_model','GameBaseModel');
        } 
        
        /**
        * Method:getByID
        * returns game object
        *
        */
        function getByID($id)
        {

          return $this->GameBaseModel->getByID($id);
          /*
          //get the game with the given id
          $query = $this->db->get_where('Games',array('ID'=>$id));
          $game = $query->row();
          
           
          if($query->num_rows()==0)
            return NULL; 

          return $this->fixGameValues($game); 
          */
        } 

        function add($info){

          if( !array_key_exists('Title',$info) ||  !array_key_exists('PlatformID',$info)){
           return false;
          }

          $this->db->insert('Games',$info);

        }

        /**
        * Method:getGamesFromLists
        * returns an array with game objects
        *
        */
        public function getGamesFromLists($listID,$order='byID',$method='a'){
            
            $this->db->select("Games.ID");
            $this->db->from("ListEntries");
            $this->db->join("Games","Games.ID=ListEntries.GameID");
            $this->db->where("ListEntries.ListID", $listID);

            if($order == 'byTitle'){
              if($method == 'd')
                  $this->db->order_by("UPPER(Title)","desc");
              else
                  $this->db->order_by("UPPER(Title)","asc");  
            }
            elseif($order == 'byPlatform'){
                
              $this->db->select('Games.* ');
              $this->db->join('Platforms','Platforms.ID=Games.PlatformID');

              if($method == 'd')
                  $this->db->order_by("UPPER(Platforms.Name)","desc");
              else
                  $this->db->order_by("UPPER(Platforms.Name)","asc"); 
            }
            else{

               if($method == 'd')
                  $this->db->order_by("ABS(ID)","desc");
               else
                  $this->db->order_by("ABS(ID)"); 
            }

            $query = $this->db->get();
            
            if($query->num_rows()==0)
               return NULL; 

            foreach ($query->result() as $game)
              $games[] = $this->getByID($game->ID);
          
            return $games; 
        }

        /**
        * Method:getAllGames
        * returns an array with game objects
        *
        */
        function getAllGames($order,$method)
        {
           
          if($order == 'byTitle'){
              if($method == 'd')
                  $this->db->order_by("UPPER(Title)","desc");
              else
                  $this->db->order_by("UPPER(Title)","asc");  
          }
          elseif($order == 'byPlatform'){
                
                $this->db->select('Games.* ');
                $this->db->join('Platforms','Platforms.ID=Games.PlatformID');

                if($method == 'd')
                    $this->db->order_by("UPPER(Platforms.Name)","desc");
                else
                    $this->db->order_by("UPPER(Platforms.Name)","asc"); 
          }
          else{

               if($method == 'd')
                  $this->db->order_by("ABS(ID)","desc");
                //ORDER BY CAST(ID AS int)
               else
                  $this->db->order_by("ABS(ID)"); 
          }


          $query = $this->db->get('Games');
           
          if($query->num_rows()==0)
            return NULL; 

          foreach ($query->result() as $game)
             $games[] = $this->fixGameValues($game); 
          
          return $games; 
        } 

        /**
        * Method:getByCoOp
        * returns array with game objects
        *
        */
        function getLastEntries($num_of_entries=5){
          $query = $this->db->query("SELECT ID FROM ( SELECT * FROM Games ORDER BY ID DESC LIMIT {$num_of_entries} ) sub ORDER BY ID ASC");
           
          if($query->num_rows()==0)
            return NULL; 

          foreach ($query->result() as $game)
             $games[] = $this->GameBaseModel->getByID($game->ID);
          
          return $games; 

        }

        /**
        * Method:getByCoOp
        * returns array with game objects
        *
        */
        function getByCoOp($co_op){

          //convert boolean to integer
          $co_op = (int) $co_op;
          $query = $this->db->get_where('Games',array('CoOp'=>$co_op));

          //if doesn't exists any game returns null
          if($query->num_rows()==0)
             return NULL;
          
          foreach ($query->result() as $game)
          {
             $games[] = $this->fixGameValues($game); 
          }
          return $games;   
        }


        /**
        * Method:getByTitle
        * returns array with game objects
        * 
        */
        function countResByTitle($title){

            $query = $this->db->query("SELECT COUNT(*) FROM Games WHERE Title COLLATE UTF8_GENERAL_CI LIKE '%{$this->db->escape_like_str($title)}%' "); 

            return $query->result_array()[0]["COUNT(*)"];    
        }


        /**
        * Method:getByTitle
        * returns array with game objects
        * 
        */
        function getByTitle($title,$limitStart=null,$limitStop=null){

            $limitStr = is_null($limitStart) || is_null($limitStop) ? "" : "LIMIT {$limitStart}," . ($limitStop - $limitStart);

            $query = $this->db->query("SELECT ID FROM Games WHERE Title COLLATE UTF8_GENERAL_CI LIKE '%{$this->db->escape_like_str($title)}%' {$limitStr}"); 

            if($query->num_rows() == 0) return NULL;

            foreach($query->result() as $game)
               $games[] = $this->GameBaseModel->getByID($game->ID);

            return $games;     
        }


        /**
        * Method:getByPublisher
        * returns array with game objects
        *
        */
        function getByPublisher($publisherName){
          
          $query = $this->db->get_where('Publishers',array('Name'=>$publisherName));
          $publisher = $query->row();

          if($query->num_rows() == 0)
            return NULL;
          //search in Games for games with publisher_id=$publisher->id
          $query = $this->db->get_where('Games',array('PublisherID'=>$publisher->ID));

          if($query->num_rows() == 0)
            return NULL;

          foreach ($query->result() as $game)
             $games[] = $this->fixGameValues($game); 
        
          return $games; 
        }


        /**
        * Method:getByDeveloper
        * returns array with game objects
        *
        */
        function getByDeveloper($developerName) {

          $query = $this->db->get_where('Developers',array('Name'=>$developerName));
          $developer = $query->row();

          if($query->num_rows() == 0)
            return NULL;

          //search in Games for games with developer_id=$developer->id
          $query = $this->db->get_where('Games',array('DeveloperID'=>$developer->ID));

          if($query->num_rows() == 0)
            return NULL;

          foreach ($query->result() as $game)
             $games[] = $this->fixGameValues($game); 
          
          return $games; 
        }


        /**
        * Method:getByPlatform
        * returns array with game objects
        *
        */
        function getByPlatform($platformName) {

          $query = $this->db->get_where('Platforms',array('Name'=>$platformName));
          $platform = $query->row();

          if($query->num_rows() == 0)
            return NULL;

          //search in Games for games with developer_id=$developer->id
          $query = $this->db->get_where('Games',array('PlatformID'=>$platform->ID));

          if($query->num_rows() == 0)
            return NULL;

          foreach ($query->result() as $game)
             $games[] = $this->fixGameValues($game); 
          
          return $games; 
        }

        /**
        * Method:changeGameTitle
        * void
        *
        */
        function changeGameTitle($id, $newTitle)
        {
          
          $this->db->set('Title', $newTitle);
          $this->db->where('ID', $id);

          $this->db->update('Games'); 
        } 

        /**
        * Method:updatePublisherID 
        * void
        *
        */
        function updatePublisherID($id, $newID){
          
          $this->db->set('PublisherID', $newID);
          $this->db->where('ID', $id);
          $this->db->update('Games');  
        } 

       /**
        * Method:updateDeveloperID 
        * void
        *
        */
        function updateDeveloperID($id, $newID){
          
          $this->db->set('DeveloperID', $newID);
          $this->db->where('ID', $id);
          $this->db->update('Games');

        }

        /**
        * Method:updatePlatformID 
        * void
        *
        */
        function updatePlatformID($id, $newID){
          
          $this->db->set('PlatformID', $newID);
          $this->db->where('ID', $id);
          $this->db->update('Games');

        }

        /**
        * Method:changeCoverImageURL (can be called for any game)
        * void
        *
        */
        function changeCoverImageURL($id, $newURL)
        {
          
          $this->db->set('CoverImageURL', $newURL);
          $this->db->where('ID', $id);

          $this->db->update('Games'); 
          
        } 

        /**
        * Method:addGameByAdmin 
        * void
        *
        */
        function addGameByAdmin($title,$url,$developer,$publisher,$platform,$description,$online,$splitScreen){

          $array = array(
                 'Title' => $title ,
                 'Description' => $description ,
                 'PlatformID' => $platform,
                 'DeveloperID'=> $developer,
                 'PublisherID'=> $publisher,
                 'CoverImageURL'=>$url
                 );

                 if(!is_null ($online))
                   $array['Online'] = $online;
          

          $data = $array;
          $this->db->insert('Games', $data); 
        }

        
        /**
        * Method:countGamesOfPlatform 
        * return an integer(number of games in this platform)
        *
        */
        function countGamesOfPlatform($platform){

            $query = $this->db->query("SELECT COUNT(*) FROM Games WHERE PlatformID={$platform}");
            return $query->result_array()[0]["COUNT(*)"];

        }

        /**
        * Method:countGamesOfDB
        * return an integer(number of games into DB)
        *
        */
        function countGames(){

            $query = $this->db->query("SELECT COUNT(*) FROM Games");
            return $query->result_array()[0]["COUNT(*)"];

        }

        function countLinkedGames(){
          $query = $this->db->query("SELECT COUNT(*) FROM Games WHERE GamesDBID IS NOT NULL");
          return $query->result_array()[0]["COUNT(*)"];
        }

        function countNativeGames(){
          $query = $this->db->query("SELECT COUNT(*) FROM Games WHERE GamesDBID IS NULL");
          return $query->result_array()[0]["COUNT(*)"];
        }

        /**
        *
        * Checks if game exists with provided options
        *
        */
        function exists($options){

            $this->db->select("");
            if( array_key_exists('GamesDBID',$options) ){
              $this->db->where('GamesDBID',$options['GamesDBID']);
            }

            $query = $this->db->get("Games");

            if($query->num_rows()==0)
              return false;
            else
              return true;

            // Todo Add Other Options
        }

        private function fixGameValues($game) {

          //get game's platform
          $query = $this->db->get_where('Platforms',array('ID'=>$game->PlatformID));
          $platform = $query->row(); 
          
          //get game's publisher
          $query = $this->db->get_where('Publishers',array('ID'=>$game->PublisherID));
          $publisher = $query->row();

          //get game's developer
          $query = $this->db->get_where('Developers',array('ID'=>$game->DeveloperID));
          $developer = $query->row();

          //Fix Some Game Values
          $fixedGame = array();
          foreach ($game as $key => $value) {

            if($key == 'PlatformID')
              $fixedGame['Platform'] = $platform;
            else if($key == 'PublisherID'){

              if(count($publisher) > 0)
                $fixedGame['Publisher'] = $publisher;
              else
                $fixedGame['Publisher'] = Null;
            }
            else if($key == 'DeveloperID'){
              if(count($developer) > 0)
                $fixedGame['Developer'] = $developer;
              else
                $fixedGame['Developer'] = Null;
            }
            else
              $fixedGame[$key] = $value;
          }

          return (object) $fixedGame;
        }

        
   }

?>
