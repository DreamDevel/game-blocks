<?php

class Beaten_Model extends CI_Model {

	function __construct()
	{
	   parent::__construct();
	   $this->beatenTable = "BeatenEntries";
	}

	public function isGameBeaten($gameID,$userID) {
		$query = $this->db->get_where($this->beatenTable,array('GameID'=>$gameID,"UserID"=>$userID));
		$list = $query->row();

		if($query->num_rows()==0)
		  return false;
		else
		  return true;
	}

	public function insertBeaten($gameID,$userID) {
		if(!$this->isGameBeaten($gameID,$userID)) {
			$data = array(
			        'GameID' => $gameID ,
			        'UserID' => $userID
			);

			$this->db->insert($this->beatenTable, $data);
			return true;
		}

		return false;
	}

	public function deleteBeaten($gameID,$userID) {

		if($this->isGameBeaten($gameID,$userID)) {
			$data = array(
		    	'GameID' => $gameID ,
		    	'UserID' => $userID
			);

	  		$this->db->delete($this->beatenTable, $data);
	  		return true;
		}

		return false;
	}
}

?>
