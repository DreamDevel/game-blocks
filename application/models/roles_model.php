<?php 

 class Roles_Model extends CI_Model{

 	function __construct(){

           parent::__construct();
        } 

     /**
      * Method:getByID
      * returns role object
      *
      */
      function getByID($id)
        {
          //get the role with the given id
          $query = $this->db->get_where('site_roles',array('id'=>$id));
          $role = $query->row();
           
          if($query->num_rows()==0)
            return NULL; 

          return $role; 
        } 

       /**
        * Method:getByName
        * returns role object
        *
        */
        function getByName($name){

          //the key Role gives the name of the role, this key will change to name
          $query = $this->db->get_where('site_roles',array('Role'=>$name));
          $role = $query->row();

          if($query->num_rows()==0)
            return NULL; 

          return $role; 

        }

       /**
        * Method:getAllRoles
        * returns array with role objects
        *
        */
        function getAllRoles()
         {
          
           $query = $this->db->get('site_roles');
           $roles = $query->result();

           if($query->num_rows()==0)
             return NULL; 

           return $roles; 
         } 

 }


?>