<?php

   class Gdgame_model extends CI_Model
   {
       
        function __construct()
        {
           parent::__construct();
           $this->table = 'GDB_Games';
        } 

        public function add($id,$gameTitle,$platform,$platformID,$releaseDate,$overview,$esrb,$developer,$publisher,$boxArtFront){


          $data = [];

          if(!is_null($id))           $data['ID']           = $id;           else  return false;
          if(!is_null($gameTitle))    $data['GameTitle']    = $gameTitle;    else  return false;
          if(!is_null($platform))     $data['Platform']     = $platform;     else  return false;
          if(!is_null($platformID))   $data['PlatformID']   = $platformID;   else  return false;

          if(!is_null($releaseDate))  $data['ReleaseDate']  = $releaseDate; 
          if(!is_null($overview))     $data['Overview']     = $overview; 
          if(!is_null($esrb))         $data['ESRB']         = $esrb; 
          if(!is_null($developer))    $data['Developer']    = $developer; 
          if(!is_null($publisher))    $data['Publisher']    = $publisher; 
          if(!is_null($boxArtFront))  $data['BoxArtFront']  = $boxArtFront; 
          // More to be added

          $this->db->insert($this->table, $data);

          return true; 
        }
        
        /**
        * Method:getByID
        * returns game object
        *
        */
        function getByID($id)
        {
          //get the game with the given id
          $query = $this->db->get_where($this->table,array('ID'=>$id));
          $game = $query->row();
          
           
          if($query->num_rows()==0)
            return NULL; 

          return $query->row(); 
        } 

        /**
        * Method:getAll
        * returns an array with game objects
        *
        */
        function getAll($options=[])
        {

          foreach($options as $field => $value){
            $this->db->where($field,$value);
          }
           
          $query = $this->db->get($this->table);

          if ($query->num_rows() > 0)
            return $query->result();
          else
            return Null;
        } 

        function getByPlatformID($platformID){
          $query = $this->db->get_where($this->table,array('PlatformID'=>$platformID));
          $games = $query->result();
          
           
          if($query->num_rows()==0)
            return NULL; 

          return $games; 
        }

        function setLinked($id,$linked){
          $this->db->set('Linked', (integer)$linked);
          $this->db->where('ID', $id);
          $this->db->update($this->table);
        }

        public function count($platformID=null){
          
          if(!is_null($platformID))
            $this->db->where('PlatformID',$platformID);

          return $this->db->count_all_results($this->table);
        }
    }
?>