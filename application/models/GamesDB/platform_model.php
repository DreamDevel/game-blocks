<?php
   class Platform_Model extends CI_Model
   {
       
        function __construct()
        {
           parent::__construct();
           $this->table = 'GDB_Platforms';
        } 


        public function add($id,$platform,$overview,$developer,$manufacturer,$cpu,$memory,$graphics,$sound,$display,$media,$maxControllers){


        	$data = [];

        	if(!is_null($id))       		$data['ID']       		= $id;  		else  return false;
        	if(!is_null($platform)) 		$data['Platform'] 		= $platform; 	else  return false;
        	if(!is_null($overview)) 		$data['Overview'] 		= $overview; 
        	if(!is_null($developer)) 		$data['Developer'] 		= $developer; 
        	if(!is_null($manufacturer)) 	$data['Manufacturer'] 	= $manufacturer; 
        	if(!is_null($cpu)) 				$data['Cpu'] 			= $cpu; 
        	if(!is_null($memory)) 			$data['Memory'] 		= $memory; 
        	if(!is_null($graphics)) 		$data['Graphics'] 		= $graphics;
        	if(!is_null($sound)) 			$data['Sound'] 			= $sound;  
        	if(!is_null($display)) 			$data['Display'] 		= $display; 
        	if(!is_null($media)) 			$data['Media'] 			= $media; 
        	if(!is_null($maxControllers)) 	$data['MaxControllers'] = $maxControllers; 

        	$this->db->insert($this->table, $data);

        	return true; 
        }

        public function getAll($options=[]){

            foreach($options as $field => $value){
              $this->db->where($field,$value);
            }

        	$query = $this->db->get($this->table);

        	if ($query->num_rows() > 0)
        		return $query->result();
        	else
        		return Null;

        }

        public function getByID($id){

            $query = $this->db->get_where($this->table,array('ID'=> $id));

            if ($query->num_rows() == 1)
                return $query->row();
            else
                return false;
        }

        public function getByName($name){
            // TODO
        }


        public function existsByID($id){
        	
        	$this->db->select("ID");
        	$this->db->where(array("ID" => $id));
        	$result = $this->db->get($this->table);

        	if ($result->num_rows() > 0)
        		return true;
        	else
        		return false;
        }

        public function existsByName($name){
        	
        	$this->db->select("SELECT 1");
        	$this->db->where(array("Platform" => $name));
        	$result = $this->db->get($this->table);

        	if ($result->num_rows() > 0)
        		return true;
        	else
        		return false;
        }

        function setLinked($id,$linked){
          $this->db->set('Linked', (integer)$linked);
          $this->db->where('ID', $id);
          $this->db->update($this->table);
        }
   }

?>