<?php

   class Gdgamesupdate_model extends CI_Model
   {
       
        function __construct()
        {
           parent::__construct();
           $this->table = 'GDB_GamesUpdate';
        } 

        public function add($id,$platformID){


          $data = [];

          if(!is_null($id))           $data['ID']           = $id;           else  return false;
          if(!is_null($platformID))   $data['PlatformID']   = $platformID;   else  return false;
          
          $this->db->insert($this->table, $data);

          return true; 
        }

        public function remove($id){
          $this->db->delete($this->table, array('ID' => $id)); 
        }

        public function getAll(){
          $query = $this->db->get($this->table);

          if($query->num_rows() > 0)
            return $query->result();
          else
            return false;
        }

        public function getPlatformsID(){

          $query = $this->db->query('SELECT DISTINCT PlatformID FROM ' . $this->table);

          if($query->num_rows() > 0){

            $IDs = [];
            foreach($query->result() as $row) {
              $IDs[] = (integer)$row->PlatformID;
            }

            return $IDs;
          }
          else
            return false;

        }

        public function countUpdates($platformID=null){
          
          if(!is_null($platformID))
            $this->db->where('PlatformID',$platformID);

          return $this->db->count_all_results($this->table);
        }

        public function exists($id){
          $this->db->select('');
          $query = $this->db->get_where($this->table, array('id' => $id));

          return ($query->num_rows() > 0 ? true : false);
        }

        public function getLastUpdateCheck(){
           $this->db->select('UNIX_TIMESTAMP(MAX(TimeFound))');
           $query = $this->db->get($this->table);

           if($query->num_rows() == 0)
            return false;
          else{
            $lastCheck = new DateTime();
            $lastCheck->setTimestamp($query->row_array()['UNIX_TIMESTAMP(MAX(TimeFound))']);
            
            if( is_null($query->row_array()['UNIX_TIMESTAMP(MAX(TimeFound))']) )
              return false;
            else
              return $lastCheck;
          }
        }
   }
?>