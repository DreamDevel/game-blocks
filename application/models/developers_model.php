<?php 

   class Developers_Model extends CI_Model{

   	   function __construct(){

   	   	 parent::__construct();
   	   }

        /**
        * Method:getAllDevelopers
        * returns array with developer objects
        *
        */
   	   function getAllDevelopers(){

   	   	  $query = $this->db->get('Developers');
   	   	  $developers = $query->result();

   	   	  if($query->num_rows()==0)
             return NULL; 

           return $developers; 
   	   }
   }

 ?>