<?php

   class List_Model extends CI_Model
   {
       
        function __construct()
        {
           parent::__construct();
        } 
        

        /**
        * Method:getByID
        * returns list object
        *
        */
        function getByID($id)
        {

          $query = $this->db->get_where('Lists',array('ID'=>$id));
          $list = $query->row();
          
           
          if($query->num_rows()==0)
            return NULL; 

          return $list;
        } 


        function getUserListByName($userID,$listName){
          $query = $this->db->get_where('Lists',array('UserID'=>$userID,'Name'=>$listName));
          $list = $query->row();
          
           
          if($query->num_rows()==0)
            return NULL; 

          return $list;
        }

        function getUserLists($userID){
          $query = $this->db->get_where('Lists',array('UserID'=>$userID));
          $lists = $query->result();
          
           
          if($query->num_rows()==0)
            return NULL; 

          return $lists;
        }


        function getListEntries($id)
        {
          $query = $this->db->get_where('ListEntries',array('ListID'=>$id));
          $entries = $query->result();
          
           
          if($query->num_rows()==0)
            return NULL; 

          return $entries;
        }

        public function createList($userID,$listName,$public = true){
          $data = array(
                  'UserID' => $userID ,
                  'Name'   => $listName , 
                  'Public' => ($public ? 1 : 0)
          );

          $this->db->insert('Lists', $data); 
        } 

        public function isEntryInList($gameID,$listID) {
          $query = $this->db->get_where('ListEntries',array('GameID'=>$gameID,'ListID'=>$listID));
          
          if($query->num_rows()==0)
            return false;
          else
            return true;
        }
        

        public function insertEntryToList($listID,$gameID){
          $data = array(
                  'GameID' => $gameID ,
                  'ListID' => $listID 
          );

          $this->db->insert('ListEntries', $data); 
        } 


        public function deleteEntryFromList($listID,$gameID){
          $data = array(
                  'GameID' => $gameID ,
                  'ListID' => $listID 
          );

          $this->db->delete('ListEntries', $data); 
        } 

        public function getlistLength($listID){
          $query = $this->db->get_where('ListEntries',array('ListID'=>$listID));
          return $query->num_rows();
        }
      
        public function isUserOwner($userID,$listID){
          $gameList = $this->getByID($listID);
          if($userID == $gameList->UserID)
            return true;
          else
            return false;
    }

   }
?>
