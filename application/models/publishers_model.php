<?php

  class Publishers_Model extends CI_Model{

 	    function __construct(){

           parent::__construct();
        } 

       /**
        * Method:getAllPublishers
        * returns array with publishers objects
        *
        */
        function getAllPublishers()
         {
          
           $query = $this->db->get('Publishers');
           $publishers = $query->result();

           if($query->num_rows()==0)
             return NULL; 

           return $publishers; 
         } 

   }
?>
