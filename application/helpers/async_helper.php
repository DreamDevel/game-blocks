<?php
	/* Will make this function global */
	function async($functionName,$params=[]){

		$command = 'nohup ' . PHP_BINDIR . "/php" . ' index.php async ' . $functionName;

		foreach($params as $param){
			$command.= ' ' . $param;
		}

		$command .= " > /dev/null 2> /dev/null &";

		async_saveState($functionName,true);

		exec($command);

		//Todo fix this, exec command returns imediatly
		//async_saveState($functionName,true);
	}

	function async_isRunning($functionName){

		return async_loadState($functionName);
	}



	function async_saveState($functionName,$state){

		$asyncDir = "./application/async/";

		$fh = fopen($asyncDir . $functionName,"w");

		fwrite($fh,(integer)$state);

		fclose($fh);

	}

	function async_loadState($functionName){

		$asyncDir = "./application/async/";

		$fh = fopen($asyncDir . $functionName,"r");

		// If file doesn't exists
		if(!$fh)
			return false;

		$state = fscanf($fh,"%d");

		fclose($fh);

		return (bool)$state[0];
	}

	/*function clearAsyncCache(){

	}*/
?>