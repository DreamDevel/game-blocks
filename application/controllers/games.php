<?php 

     class Games extends MY_Controller{

          public function __construct(){

          	parent::__construct();
          	$this->load->model('game_model','GameModel');
            $this->load->model('tank_auth/users');
            $this->load->model('list_model','lists');

          }

          public function index(){

            $this->load->model('beaten_model','BeatenModel');
            $data['isUserMember']  = false;
            $data["numberOfPages"] = 0;

		        $title  = $this->input->get('title');
            $title = trim($title);


            $page   = $this->input->get('p') && $this->input->get('p') > 0 ? $this->input->get('p') : 1;
		        $games  = null;
            $gamesPerPage = 15;
            
            if($title){

              $data['searchInput'] = $title;
              // setting the amount of games we want per page 
              $limitStart = $gamesPerPage * ($page-1);
              $limitStop  = $limitStart+$gamesPerPage;

              $games = $this->GameModel->getByTitle($title,$limitStart,$limitStop);

              if($games){

                //setting the number of pages
                $gamesInDB = $this->GameModel->countResByTitle($title);
                $numberOfPages = $gamesInDB / $gamesPerPage;

                $data["numberOfPages"] = ceil($numberOfPages);
              } 

            }

            // if user is logged in show if games are in collection
            if($this->tank_auth->is_logged_in(true)){

                $userID = $this->tank_auth->get_user_id();
                $user   = $this->users->get_user_by_id($userID,true);
                $collection = $this->lists->getUserListByName($userID,'Collection');

                $data['user']  = $user;
                $data['isUserMember'] = true; 

                $gamesInCollection = array();

                if ($games!=null){
                   foreach($games as $game)
                     $gamesInCollection[$game->ID] = $this->lists->isEntryInList($game->ID,$collection->ID);
                   
                   $data['inCollection'] = $gamesInCollection;

                }
              
            }

                 
            $data['games'] = $games;
            if($games){
              foreach($data['games'] as $game) {
                $game->isBeaten = $this->BeatenModel->isGameBeaten($game->ID,$userID);
              }
            }

            $this->load->view('templates/header');
			      $this->load->view('games',$data);
			      $this->load->view('templates/footer');

	      }

     

     }


?>