<?php

class Profile extends MY_Controller{

	private $user;

	public function __construct(){
		parent::__construct();
		
		// Get Current User
		$this->user = $this->tank_auth->get_current_user();
		if(is_null($this->user)) {
			redirect('');
		}
	}

	public function index(){
		echo "This is the index Of Profile Page";

	}

	public function myLists(){
		$this->load->model('list_model','ListModel');

		$data['lists'] = $this->ListModel->getUserLists($this->user->id);

		$this->load->view("templates/header");
		$this->load->view("profile/lists",$data);
		$this->load->view("templates/footer");
	}
}

?>