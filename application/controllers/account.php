<?php 

  class Account extends MY_Controller{

  	public function __construct(){

  		parent::__construct();
  		$this->load->config('tank_auth', TRUE);
  	}

  	public function settings(){

        
        //get current user
  		$user = $this->tank_auth->get_current_user();
  		if($user){

        	$newPassword  = $this->input->post('newPas');
        	$passwordConf = $this->input->post('passwordConf');
        	$currentPas   = $this->input->post('currentPas');
        	$newEmail     = $this->input->post('email');

        	$data["notValidPass"]    = false;
        	$data["passDoesntMatch"] = false;
        	$data["wrongPass"]       = false;
        	$data["notAvailable"]    = false;

       	    if($newPassword && $passwordConf){  

          	    if($newPassword == $passwordConf){

           		   if(strlen($newPassword)>=6){

              		//change user's password 
              		$isPasswordChanged = $this->tank_auth->change_password($currentPas,$newPassword);
              		if(!$isPasswordChanged)
                 	   $data["wrongPass"] = true;
          		   }
          		   else 
          			  $data["notValidPass"] = true;
          		 }
           		else 
          			$data["passDoesntMatch"] = true;	
        	}

        	if($newEmail){
              
               $isMailAvailable = $this->tank_auth->is_email_available($newEmail);

               if(!$isMailAvailable)
               	$data["notAvailable"] = true;
               else 
               {
               	    //if current password is correct change user's email
               	    $result = $this->tank_auth->set_new_email($newEmail, $currentPas);

                    if(!$result){

                 		//if error for wrong pass is false change to true 
                    	if(!$data["wrongPass"])
                    		$data["wrongPass"] = true;
                     }
                     else{

                     $emailActivation = $this->config->item('email_activation', 'tank_auth');
                     // If email_activation is false
                     if(!$emailActivation)
                          $this->tank_auth->activate_new_email($result['user_id'],$result['new_email_key']);
                     }
               }
        	}

  		    $this->load->view('templates/header');
		    $this->load->view('account/settings',$data);
		    $this->load->view('templates/footer');
        }
        else 
        	show_404();
  	}
  }

?>