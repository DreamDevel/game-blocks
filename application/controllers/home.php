<?php

class home extends MY_Controller {

    public function index()
    {
    	$this->load->model('game_model','gameModel');
    	$data['lastGames'] = $this->gameModel->getLastEntries(7);

        $this->load->view('templates/header');
        $this->load->view('home',$data);
        $this->load->view('templates/footer');
    }
}

?>