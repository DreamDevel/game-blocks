<?php 


  class Platforms extends MY_Controller{

    public function __construct(){

        parent::__construct();
        $this->load->model('platforms_model','platforms');
    }

     public function index(){

     	$platforms = $this->platforms->getAllPlatforms();
     	$data['platforms'] = $platforms;

        $this->load->view('templates/header');
	    $this->load->view('platforms',$data);
	    $this->load->view('templates/footer');
     }

 }
?>