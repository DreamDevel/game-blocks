<?php 

class Admin extends MY_Controller{

	public function __construct(){

		parent::__construct();
		$this->load->model('tank_auth/users');
		$this->load->model('roles_model','roles');
		$this->load->model('game_model','games');
		$this->load->model('publishers_model','publishers');
		$this->load->model('developers_model','developers');
		$this->load->model('platforms_model','platforms');
			
		$user = $this->tank_auth->get_current_user();

		// Get user Role
		$userRole;
		if(is_null($user)) 
			$userRole = '';
		else
			$userRole = $this->roles->getByID($user->role_id)->name;

		// Give access only to administrators
		if($userRole != "Administrator")
			show_404();
	}

	public function index(){

		$this->load->view('templates/header');
		$this->load->view('admin/home');
		$this->load->view('templates/footer');
	}

	public function usersList(){
		
		$data['users'] = $this->users->get_all_users();

        $this->load->view('templates/header');
		$this->load->view('admin/usersList',$data);
		$this->load->view('templates/footer');
	}

	public function user($id){
		
        $data['user']  = $this->users->get_user_by_id($id);
        $data['role']  = $this->roles->getByID($data['user']->role_id);
        $data['roles'] = $this->roles->getAllRoles();


		$this->load->view('templates/header');
		$this->load->view('admin/user',$data);
		$this->load->view('templates/footer');

	}
    
    //User functions
	public function banUser($id){

      	$ban_reason = $this->input->post('ban_reason');

      	if ($ban_reason!=null)
     		 $this->tank_auth->ban_user_by_admin($id, $ban_reason); 
      	 
      	redirect("admin/user/{$id}");
	}

	public function unbanUser($id){

      	$this->tank_auth->unban_user_by_admin($id);
      	redirect("admin/user/{$id}");
	}


	public function deleteUser($id){

      	$this->tank_auth->delete_user_by_admin($id);
      	redirect("admin/usersList");
	}

	public function changeUserPassword($id){
       
        $new_pass = $this->input->post('new_password');
        if ($new_pass!=null)
           $this->tank_auth->change_password_by_admin($id,$new_pass);
        
      	redirect("admin/user/{$id}");
	}

	public function changeUserRole($id, $role_id){
		
        $this->users->update_role($id, $role_id);
        redirect("admin/user/{$id}");
	}

	public function activateUser($id){

		$this->users->activate_user_by_admin($id);
		redirect("admin/user/{$id}");
	}

	public function gamesList($order="byID",$method='a'){
		
		if(($order == "byID" || $order == "byTitle" || $order =="byPlatform") && ($method == 'a' || $method == 'd'))
           $data['games'] = $this->games->getAllGames($order,$method);
        else
        	show_404();
        
    $this->load->view('templates/header');
		$this->load->view('admin/gamesList',$data);
		$this->load->view('templates/footer');
	}

	public function game($id){
 
        $data['game']       = $this->games->getByID($id);
        $data['platforms']  = $this->platforms->getAllPlatforms();
        $data['publishers'] = $this->publishers->getAllPublishers();
        $data['developers'] = $this->developers->getAllDevelopers();
 
		$this->load->view('templates/header');
		$this->load->view('admin/game',$data);
		$this->load->view('templates/footer');
	}
    
    //Game functions
	public function changeGameTitle($id){
       
       $newTitle = $this->input->post('newTitle');

       if ($newTitle!=null)
          $this->games->changeGameTitle($id,$newTitle);
       
       redirect("admin/game/{$id}");
	}

	public function changePlatformID($id){

		$newID = $this->input->post('newID');

		if($newID!=null)
           $this->games->changePlatformID($id,$newID);

        redirect("admin/game/{$id}");
	}

    public function changeCoverImageURL($id){

    	$newURL = $this->input->post('newURL');

    	if($newURL!=null)
    		$this->games->changeCoverImageURL($id, $newURL);

    	redirect("admin/game/{$id}");
    }

    public function changeGamePublisher($id, $publisherID){

        $this->games->updatePublisherID($id, $publisherID);
        redirect("admin/game/{$id}");

    }

    public function changeGameDeveloper($id, $developerID){

    	$this->games->updateDeveloperID($id, $developerID);
    	redirect("admin/game/{$id}");
    }

    public function changeGamePlatform($id, $platformID){
        
    	$this->games->updatePlatformID($id, $platformID);
    	redirect("admin/game/{$id}");
    }

    public function searchBar(){

    	$searchItem = $this->input->get('item');
    
    	if($searchItem!=null)
    	{
           $userByName  = $this->users->get_user_by_username($searchItem);
           $userByEmail = $this->users->get_user_by_email($searchItem);

           if($userByName!=NULL)
           	 redirect("admin/user/{$userByName->id}");  
           else if ($userByEmail!=NULL)
           	 redirect("admin/user/{$userByEmail->id}");
           else 
           	 echo "No user with {$searchItem} username";   	 
    	}


    }

    public function formToAddGame(){

     $data['platforms']  = $this->platforms->getAllPlatforms();
     $data['publishers'] = $this->publishers->getAllPublishers();
     $data['developers'] = $this->developers->getAllDevelopers();
 
     $this->load->view('templates/header');
     $this->load->view('admin/addGame',$data);
     $this->load->view('templates/footer');
    }

    public function addGame(){

      $title         = $this->input->post('title');
      $url           = $this->input->post('url');
      $platformID    = $this->input->post('platform');
      $developerID   = $this->input->post('developer');
      $publisherID   = $this->input->post('publisher');
      $description   = $this->input->post('description');
      $online        = $this->input->post('online');
      $splitScreen   = $this->input->post('splitScreen');

      if($online == "Null")
      	$online = null;
      if($splitScreen == "Null")
      	$splitScreen = null;

      $this->games->addGameByAdmin($title,$url,$developerID,$publisherID,$platformID,$description,$online,$splitScreen);
      echo 'game added successfully';
    }


    public function GamesDB($subView='index',$action='none'){

      if($subView=='index') {
        $this->load->view('templates/header');
        $this->load->view('admin/GamesDB/main');
        $this->load->view('templates/footer');
      }
      elseif($subView == 'Platforms'){
        $this->load->view('templates/header');

        $data = $this->_gamesDBPlatforms($action);

        $this->load->view('admin/GamesDB/platforms',$data);
        $this->load->view('templates/footer');
      }
      elseif($subView == 'Games'){
        $this->load->view('templates/header');

        $data =  $this->_gamesDBGames($action);
        $this->load->view('admin/GamesDB/games',$data);
        $this->load->view('templates/footer');
      }
      elseif($subView == 'Settings'){
        $this->load->view('templates/header');
        $this->load->view('admin/GamesDB/settings');
        $this->load->view('templates/footer');
      }
      else {
        show_404();
      }

    }


    private function _gamesDBPlatforms($action){
        // Load GamesDBLib
      $this->load->library('GDBLib');


      if($action =='checkForUpdate'){

        $platformUpdates = $this->gdblib->checkForPlatformUpdates();
        $data['updatesCount'] = (is_null($platformUpdates) ? 0 : count($platformUpdates));
      }
      elseif($action == 'update'){
        $platformsUpdated = $this->gdblib->updatePlatforms(true);
        $data['platformsUpdated'] = (is_null($platformsUpdated) ? 0 : $platformsUpdated);

      }
      elseif($action == 'LinkAll'){
        $this->gdblib->linkDB->linkPlatforms();
      }
      
      $data['platforms'] = $this->gdblib->localDB->getAllPlatforms();
      $data['unlinkedPlatforms'] = count($this->gdblib->linkDB->getUnlinkedPlatforms());
      return $data;
        // If no action show basic page
        // if action== checkForUpdate check for update and update platform
            // Return ajax
        // If action== update, update and
            // Return ajax success and changes
    }

    private function _gamesDBGames($action){
      $this->load->library('GDBLib');
      $this->load->model('game_model','GBGameModel');
      $this->load->helper('async');

      $data = null;

      if($action =='UpdateCheck'){
        $this->gdblib->checkForNewGames();

        redirect(site_url('admin/GamesDB/Games/'));
      }
      elseif($action == 'Update'){
        
        if(!async_isRunning("updateGDBGames")){
          async('updateGDBGames');
        }

        redirect(site_url('admin/GamesDB/Games/'));
        
      }
      elseif($action == 'LinkAll'){
        $this->gdblib->linkDB->linkGames();

        redirect(site_url('admin/GamesDB/Games/'));
      }
      elseif($action == 'Async-MissingGames'){
        echo $this->gdblib->localDB->countNewGames();
        exit();
      }
      elseif($action == 'Async-GamesInDB'){
        echo $this->gdblib->localDB->countGames();
        exit();
      }

      $data['updating']        = async_isRunning("updateGDBGames");

      $data['lastUpdateCheck'] = $this->gdblib->localDB->getLastGamesUpdateCheck();
      $data['missingGames']    = $this->gdblib->localDB->countNewGames();
      $data['dbGames']         = $this->gdblib->localDB->countGames();
      //$data['unlinkedGames']   = $this->gdblib->linkDB->countUnlinkedGames();
      $data['linkedGames']     = $this->GBGameModel->countLinkedGames();

      // Get All Platforms
      $platforms = $this->gdblib->localDB->getAllPlatforms();
      if($platforms) {
        foreach($platforms as $platform){

          $platform->numberOfNewGames = $this->gdblib->localDB->countNewGames($platform->getID());
          $platform->numberOfLocalGames = $this->gdblib->localDB->countGames($platform->getID());

        }
      }

      $data['platforms'] = $platforms; 

      // TODO Display New Games On Click
      $data['newGamesList'] = $this->gdblib->localDB->getNewGames();

      return $data;
    }
	
}


?>
