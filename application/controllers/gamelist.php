<?php 

	class GameList extends MY_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->model('list_model','ListModel');
		}


		public function index($id=0,$order="byID",$method='a') {

			// Get list by id
			$gameList = $this->ListModel->getByID($id);
			$userID = $this->tank_auth->get_user_id();

			// If list doesn't exist display custom 404
			if($gameList != null) {

				$privileges = new stdClass();
				$data['isUserMember']  = true;
				// Show lists only to members
				if($this->tank_auth->is_logged_in(true)) {

					if($this->_isUserOwner($gameList))
					{
						
						$privileges->edit = true;
						$privileges->view  = true;
						//= "This is your list, You have rights to see it and edit it";
					}
					else{
						$privileges->edit = false;
						if($gameList->Public)
							$privileges->view  = true;
						else
							$privileges->view  = false;
					}

					// Get Entries
					if($privileges->view){
						
					   $this->load->model('game_model','GameModel');
					   $this->load->model('beaten_model','BeatenModel');
					   if(($order == "byID" || $order == "byTitle" || $order =="byPlatform") && ($method == 'a' || $method == 'd')){

                          $data['games'] = $this->GameModel->getGamesFromLists($gameList->ID,$order,$method);

                          // Add isBeaten value to every game
                          foreach($data['games'] as $game) {
                          	$game->isBeaten = $this->BeatenModel->isGameBeaten($game->ID,$userID);
                          }
                          $data['gameList']  = $gameList;
                       }
                       else
        	              show_404();
					}

				}
				else
				{
					$data['isUserMember']  = false;
				}

				$data['privileges'] = $privileges;


				$this->load->view('templates/header');
				$this->load->view('gamelist',$data);
				$this->load->view('templates/footer');

			}
			else { 

				show_404();
			}

		}

		// Post Function
		// TODO Function Needs User Authedication  ListID 
		public function addGame($gameID,$listID=-1){


			// Authedicate User
			if($this->tank_auth->is_logged_in()){
				$user = $this->tank_auth->get_current_user();

				$this->load->model('list_model','listModel');
				if($listID == -1)
					$gameList = $this->listModel->getUserListByName($user->id,"Collection");
				else 
					$gameList = $this->listModel->getByID($listID);

				if(!is_null($gameList)){
					if($this->_isUserOwner($gameList)){
						// Check if game is already in the list TODO
							
						// If It is owner Add To Your Games
						$this->listModel->insertEntryToList($gameList->ID,$gameID);

						$numOfGames = $this->listModel->getlistLength($gameList->ID);
						$jsonObj = array('action'=>"success",
										 'numberOfGames'=>$numOfGames);

						echo json_encode($jsonObj);
						
					}
					else{
						$jsonObj = array('action'=>"error",
										 'errorType'=>"WrongPrevileges");
						echo json_encode($jsonObj); 
					}
				}
				else{
					$jsonObj = array('action'=>"error",
										 'errorType'=>"ListDoesNotExist");
					echo json_encode($jsonObj);
				}
			}
			else{
				$jsonObj = array('action'=>"error",
										 'errorType'=>"NotAMember");
				echo json_encode($jsonObj);
			}

		}

		public function removeGame($gameID,$listID=-1){


			// Authedicate User
			if($this->tank_auth->is_logged_in()){
				$user = $this->tank_auth->get_current_user();

				$this->load->model('list_model','listModel');
				if($listID == -1)
					$gameList = $this->listModel->getUserListByName($user->id,"Collection");
				else 
					$gameList = $this->listModel->getByID($listID);

				if(!is_null($gameList)){
					if($this->_isUserOwner($gameList)){
						// Check if game is in the list TODO
							
						// If It is owner Remove Game If Exist
						$this->listModel->deleteEntryFromList($gameList->ID,$gameID);
												$numOfGames = $this->listModel->getlistLength($gameList->ID);
						$jsonObj = array('action'=>"success",
										 'numberOfGames'=>$numOfGames);

						echo json_encode($jsonObj);
						
					}
					else{
						$jsonObj = array('action'=>"error",
										 'errorType'=>"WrongPrevileges");
						echo json_encode($jsonObj);
					}
				}
				else{
					$jsonObj = array('action'=>"error",
										 'errorType'=>"ListDoesNotExist");
					echo json_encode($jsonObj);
				}
			}
			else{
				$jsonObj = array('action'=>"error",
										 'errorType'=>"NotAMember");
				echo json_encode($jsonObj);
			}

		}


		public function length($listID=-1){

			// Authedicate User
			if($this->tank_auth->is_logged_in()){
				$user = $this->tank_auth->get_current_user();

				$this->load->model('list_model','listModel');
				if($listID == -1)
					$gameList = $this->listModel->getUserListByName($user->id,"Collection");
				else 
					$gameList = $this->listModel->getByID($listID);

				if(!is_null($gameList)){
					if($this->_isUserOwner($gameList)){

						$numOfGames = $this->listModel->getlistLength($gameList->ID);
						$jsonObj = array('action'=>"success",
										 'length'=>$numOfGames);

						echo json_encode($jsonObj);
						
					}
					else{
						$jsonObj = array('action'=>"error",
										 'errorType'=>"WrongPrevileges");
						echo json_encode($jsonObj);
					}
				}
				else{
					$jsonObj = array('action'=>"error",
										 'errorType'=>"ListDoesNotExist");
					echo json_encode($jsonObj);
				}
			}
			else{
				$jsonObj = array('action'=>"error",
										 'errorType'=>"NotAMember");
				echo json_encode($jsonObj);
			}
		}

		private function _isUserOwner($gameList){
			$userID = $this->tank_auth->get_user_id();
			if($userID == $gameList->UserID)
				return true;
			else
				return false;
		}

	}
?>