<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


// TODO Check if this is running in curl or something similar
class Async extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		// This Controller is only available via CLI
		if(!$this->input->is_cli_request())
			show_404();
	}


	function updateGDBGames(){

		// Temporary
		$fullUpdate = false;

		$this->load->library("GDBLib");


		if(!$fullUpdate){

		    // Get The List Of New Games
		    $newGamesList = $this->gdblib->localDB->getNewGames();

		    foreach($newGamesList as $game){

		        $gameObj = $this->gdblib->onlineDB->getGameByID($game->ID);

		        $this->gdblib->localDB->addGame(
		            $gameObj->getID(),
		            $gameObj->getGameTitle(),
		            $gameObj->getPlatform(),
		            $gameObj->getPlatformID(),
		            $gameObj->getReleaseDate(),
		            $gameObj->getOverview(),
		            $gameObj->getESRB(),
		            $gameObj->getDeveloper(),
		            $gameObj->getPublisher(),
		            (isset($gameObj->getBoxArts()['front']) ? $gameObj->getBoxArts()['front'] : null)
		            );
		       
		       $this->gdblib->localDB->removeNewGame($game->ID);

		    }
		}

		// Temporary Fix for saving async operation state
		$this->load->helper('async');
		async_saveState('updateGDBGames',false);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */