<?php

	class Game extends MY_Controller{

		public function __construct(){
			parent::__construct();

			$this->load->model('game_model','GameModel');
			$this->load->model('gameviews_model','GameViewsModel');
		}


		public function index($id=0) {

			// Get game by id
			$game = $this->GameModel->getByID($id);

			// If game doesn't exist display custom 404
			if($game != null) {

				$this->load->model('list_model','listModel');
				$userID = $this->tank_auth->get_user_id();
				$data['gameInCollection'] = $this->listModel->isEntryInList($game->ID,$userID);

				//$this->load->model("beaten_model","beatenModel");
				//$this->beatenModel->deleteBeaten($game->ID,$userID);
				//Load View
				$data['game']     = $game;
				$this->load->view('templates/header');
				$this->load->view('game',$data);
				$this->load->view('templates/footer');

				if($userID!=NULL){

					//if user is logged in check for views in the last 24 hours
					$isViewed = $this->GameViewsModel->hasGameViewedByUser($userID,$id);
					if($isViewed == 0)
					  $this->GameViewsModel->add($id,$userID);
				}

			}
			else {

				show_404();
			}

		}

		public function beaten() {

			if($this->tank_auth->is_logged_in()){
				$userID = $this->tank_auth->get_user_id();

				$this->load->model("beaten_model","beatenModel");
				$gameID = $this->input->post("gameID");
				$beaten = $this->input->post("beaten");

				 //Check if gameid exists TODO
				if( !$gameID )
					echo json_encode(array('action'=>"error",'errorType'=>"NoGameID"));
				else {
					$success = false;
					if($beaten == "true")
						$success = $this->beatenModel->insertBeaten($gameID,$userID);
					else
						$success = $this->beatenModel->deleteBeaten($gameID,$userID);

					if($success)
						echo json_encode(array('action'=>"success"));
					else
						echo json_encode(array('action'=>"error",'errorType'=>"Unknown"));
				}
			}

		}

	}
?>
