<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('tank_auth');
	}

	function index()
	{
		//$this->load->model('gamebase_model','gameBaseModel');
		//echo print_r($this->gameBaseModel->getByID(3));

		//$this->load->model('game_model','gameModel');
		//echo "Exists: " . $this->gameModel->exists(array("GamesDBID"=>0));

		//$this->load->model('platforms_model','platformsModel');
		//echo "Exists: " . $this->platformsModel->exists(array("GamesDBID"=>0));

		$this->load->library('GDBLib');

		$this->gdblib->linkDB->linkGames();

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */