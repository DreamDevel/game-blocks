<?php $this->load->view("templates/gameblock"); ?>
<?php $view_type = "blocks" ?>
<?php if($this->uri->segment(4)){

      if($this->uri->segment(5)=='a')
         $method = 'd';
      else
         $method = 'a';
    }
    else
       $method = 'd';
 ?>

<div style="color:white;margin:20px;">
<?php if($isUserMember) : ?>
	<strong>Privileges:</strong> <br>
	Edit : <?= $privileges->edit ? "YES" : "NO" ?> <br>
	view : <?= $privileges->view ? "YES" : "NO" ?> <br><br>

  <!-- Table View -->
	<?php if($privileges->view == true) : ?>
      <?php if($games != 0) :?>

        <?php if($view_type == "list") : ?>
      <table style="margin:0 auto;">
      <thead>
        <tr>
          <th>Link</th>

           <th><a href=<?=site_url("gamelist/index/{$gameList->ID}/byID/{$method}")?>>ID</a></th>
           <th><a href=<?=site_url("gamelist/index/{$gameList->ID}/byTitle/{$method}")?>>Title</a></th>
           <th><a href=<?=site_url("gamelist/index/{$gameList->ID}/byPlatform/{$method}")?>>Platform</a></th>

        </tr>
     </thead>
     <tbody>

       <?php foreach($games as $game) : ?>
		 <tr>
          <td> <a href= <?= site_url("game/index/{$game->ID}");?> ><img src=<?= base_url("images/admin/view.png")?> /></a> </td>
			    <td> <?= $game->ID ?> </td>
			    <td> <?= $game->Title->Value ?> </td>
			    <td> <?= $game->Platform->Value->Name ?> </td>

			    <?php if($privileges->edit == true):?>
         	  <td><button onclick=<?= "ListManager.removeGame({$game->ID},-1);"?>>Remove</button> </td>
          <?php endif;?>
		 </tr>

      <?php endforeach; ?>

     </tbody>
   </table>

   <?php elseif($view_type == "blocks") : ?>
   <!-- Blocks View -->
   <div style="width:95%;margin-left:auto;margin-right:auto;text-align:center;">
   <div style="text-align:right;margin-right:50px;margin-bottom:18px;font-size: 0.84064rem;">
      Order By :
      <a href=<?=site_url("gamelist/index/{$gameList->ID}/byID/{$method}")?>>ID</a> -
      <a href=<?=site_url("gamelist/index/{$gameList->ID}/byTitle/{$method}")?>>Title</a> -
      <a href=<?=site_url("gamelist/index/{$gameList->ID}/byPlatform/{$method}")?>>Platform</a>
   </div>
   <div style="text-align:right;margin-right:50px;margin-bottom:18px;font-size: 0.84064rem;"> <a href="javascript:void(0)" onClick="clearRemoved()">Clear Removed</a></div>
    <?php foreach($games as $game) {
      echo_gameBlock($game,true);
    } ?>
  </div>
  <?php endif; ?>


   <?php else: ?>
   <?="No games in your list"?>
   <?php endif;?>
 <?php else:?> <?="This list is private" ?>
 <?php endif; ?>

<?php else : ?>
	This area is only for members of our site.<br>
	If you are a member please login in by clicking at the upper right corner.<br>
	If you are not yet a member you can register.<br>
<?php endif; ?>

</div>

<script>

$(document).ready(function(){
        var siteurl = <?= '"' . site_url()  . '"' ?>;
  ListManager.initialize(siteurl);
    /* $(document).on('GameAddedToList',function(event,listID){
      var listLength = ListManager.length(listID);
      Notifier.notifyListAction(listLength,'add');
    });

    $(document).on('GameRemovedFromList',function(event,listID){
      var listLength = ListManager.length(listID);
      Notifier.notifyListAction(listLength,'remove');
    });*/
});
</script>

<script type="text/javascript">
  
  function clearRemoved() {
    $(".gameblock").each(function(){
     if( $(this).find(".own_status").attr('owner') == "false") {
        $(this).hide(70);
     }
    });
  }
</script>

<script src=<?= base_url('/js/ListManager.js') ?> ></script>
<script src=<?= base_url('/js/Notifier.js')    ?> ></script>

