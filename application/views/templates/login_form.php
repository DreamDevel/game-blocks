<?php

// ----- Error Reporting Via Flashdata ------ //

$loginErrorOccured = $this->session->flashdata('loginError') == "true" ? true : false;
$errorMessage = "";
$useCaptcha = false;
// Check for Login Error where we may use captcha
if($loginErrorOccured){
  $errorMessage = $this->session->flashdata('errorMessage');
  $useCaptcha = $this->session->flashdata('useCaptcha');
}
else {
  // Check for captcha
  if(  $this->tank_auth->is_max_login_attempts_exceeded('') )
    $useCaptcha = true;
}


// ----------- Captcha ----------- //
$captchaCode = '';

if($useCaptcha) {
  $this->load->helper('recaptcha');
  // Add custom theme so we can get only image
  $options = "<script>var RecaptchaOptions = {theme: 'custom', custom_theme_widget: 'recaptcha_widget'};</script>\n";

  // Get reCAPTCHA JS and non-JS HTML
  $html = recaptcha_get_html($this->config->item('recaptcha_public_key', 'tank_auth'));
  $captchaCode = $options.$html;
}


?>

<div id="LoginRegisterModal" class="reveal-modal" data-reveal>

<!-- TEMPORARY BG IMAGE -->
  <div style="position:absolute;right:49%;bottom:0;z-index:-1;opacity:0.5;-webkit-transform: scaleX(-1);"><img src=<?= base_url('images/Kratos.png') ?> /></div>
  <!--  -->

  <div style="border-right: 1px solid #ccc;float:left;width:47%">
  	<h2>New Account</h2>
  	<p class="lead">Not a member yet ?</p>
    <p>
      <ul>
        <li>Grand access to games, platforms and groups</li>
        <li>Create your personal game lists</li>
        <li>Keep track of your finished games</li>
        <li>Join our gaming community </li>
      </ul>
   </p>
  	<p><?= anchor('auth/register','Register For Free') ?></p>
  </div>

  <div style="margin-left:4px;float:right;width:50%">
  	<h2>Login</h2>
  	<p class="lead">Are you already a member ? </p>
  	<p>
  		<form class="custom" action=<?= site_url("auth/login") ?> method="post" data-abide >
  			
      <!-- Username Input -->
        <div class="name-field">
  				<label>Username or Email</label>
  				<input name="login" type="text" placeholder="Your Username or Email" required>
          <small class="error">
          Username is required"
          </small>
  			</div>

        <!-- Password Input -->
  			<div class="password-field">
  				<label>Password</label>
  				<input name="password" type="password" placeholder="Your Password" pattern="" required>
          <small class="error">
          Password is required. It must also be at least 6 characters long
          </small>
  			</div>
        <input type="hidden" name="redirectURL" value=<?= uri_string() ?> >


        <?php if ($useCaptcha) : ?>
        <div id="recaptcha_widget" style="display:none">
          <table>
            <tr>
              <td>
                <div id="recaptcha_image" style="width:300px;height:57px;overflow:hidden;"></div><br />
              </td>
              <td>
                <div><a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a></div>
                <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
                <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
              </td>
          </tr> 
        <table>

        <div class="alpha_numeric-field">
        <label><div class="recaptcha_only_if_image">Enter the words above</div><div class="recaptcha_only_if_audio">Enter the numbers you hear</div></label>
        <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" required >
        <small class="error">
          Captcha code is required
        </small>
        </div>
      </div>
    <?php endif; ?>

    <div>
      <input id="remember" name="remember" type="checkbox" value='1'>
      <label>Remember Me</label>
    </div>
        
        <?php // Display Error Returned After Data validation 
          if($loginErrorOccured) { ?>     
        <div data-alert class="alert-box warning round text-center">
          <?= $errorMessage;  ?>
        <a href="#" class="close">&times;</a>
        </div>
        <?php } ?>
  			
        <button type="submit">Submit</button>
  		</form>
  	</p>
    <div class="right">
    <p>
    <?= anchor('auth/forgot_password','Forgot my password') ?>
    </p>
    </div>
  </div>
  
  <a class="close-reveal-modal">&#215;</a>
</div>	

    <?php if($loginErrorOccured OR strlen(validation_errors()) > 0) { ?>
    <script>
      $(document).ready(function(){
        $( "#loginButton" ).trigger( "click" );
      });
    </script>
    <?php } ?>

    <?= $captchaCode ?>