<?php

// Get Web Site Name
$site_name = $this->config->item('website_name', 'tank_auth');

$user = $this->tank_auth->get_current_user();

?>
<!DOCTYPE html>
<html>
	<head>
		<title><?= $site_name ?></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <script src=<?= base_url('js/libs/jquery.js')   ?>> </script>
    <script src=<?= base_url('js/libs/jquery.gritter.min.js')   ?>> </script>
    <script src=<?= base_url('js/libs/modernizr.js') ?>> </script>

    <link rel="stylesheet" type="text/css" href=<?= base_url('css/foundation.css') ?> />
    <link rel="stylesheet" type="text/css" href=<?=base_url('css/jquery.gritter.css')?> />
	<link rel="stylesheet" type="text/css" href=<?=base_url('css/style.css')?> />
    <?php if($this->tank_auth->is_logged_in()) : ?>
        <?php if($user->role_id == 2): ?>
            <link rel="stylesheet" type="text/css" href=<?=base_url('css/admin.css')?> />
        <?php endif; ?>
    <?php endif; ?>
    <link rel="icon" 
      type="image/ico" 
      href="<?= base_url('images/favicon.ico') ?>" />
	</head>
	<body>

<?php $this->load->view('templates/top_bar');
