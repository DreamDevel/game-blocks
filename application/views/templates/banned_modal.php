<?php  if($this->session->flashdata('banned') == "true") : ?>
<script>
	$(document).ready(function(){
		$('#bannedModal').foundation('reveal','open');
});
</script>



<div id="bannedModal" class="reveal-modal small" data-reveal>
  <p><h3><img src="http://icons.iconarchive.com/icons/kyo-tux/soft/128/Delete-icon.png" style="float:left; width:50px; margin-right:15px;" />You have been banned</h3></p>
  <p>Your account is disabled. If you need more info about this action please contact administrators via email. <a href="#">Contact Form</a></p>
  <a class="close-reveal-modal">&#215;</a>
</div>
<?php endif; ?>