<?php  if($this->session->flashdata('inactive') == "true") : ?>
<script>
	$(document).ready(function(){
		$('#inactiveModal').foundation('reveal','open');
});
</script>



<div id="inactiveModal" class="reveal-modal small" data-reveal>
  <p><h3><img src="http://icons.iconarchive.com/icons/kyo-tux/soft/128/Alert-icon.png" style="float:left; width:50px; margin-right:15px;" />Your Account is inactive</h3></p>
  <p>Your haven't activated your account yet. Please check your email for the activation link. If you can't find an email from us click this link: <a href=<?= site_url('auth/send_again') ?> >Send activation mail again</a></p>
  <a class="close-reveal-modal">&#215;</a>
</div>
<?php endif; ?>