<?php /*
*  Description
*  GameBlock Needs $inCollection boolean associative array and $game object
*/ ?>


<?php /* Echo Functions */ ?>

<?php function echo_gameBlock($game,$inCollection) { ?>
<div class='gameblock' gameID="<?= $game->ID ?>">
  <div class='shadow_line'></div>
  <!-- Top Bar -->
  <div class='header'>
    <!-- In Collection Status -->
    <div class='own_status_full' owner="<?= $inCollection ? 'true' : 'false' ?>" pending="false" onClick="changeOwnStatus(this)"><?= $inCollection ? 'Remove' : 'I Own It' ?></div><!--
 --><div class='own_status' owner="<?= $inCollection ? 'true' : 'false' ?>"></div>
    <!-- Title -->
    <div class='title' onClick="window.location='<?= site_url('game/index/' . $game->ID) ?>'">
    <?= strlen($game->Title->Value) <= 38 ? $game->Title->Value : (substr($game->Title->Value, 0,38) . "...") ?>
    </div>
  </div>
  <!-- Information Area -->
  <div class='information'>
    <!-- Top Line Shadow -->
    <div class='shadow_line'></div>
    <!-- Background Image -->
    <div class='background-image' style="background-image:url('<?= base_url("images/gameblock-bg.jpg") ?>');"></div>
    <!-- Cover Image -->
    <div class='cover'><img src='<?= $game->CoverImage ? $game->CoverImage->Value : base_url("images/no-cover.png") ?>'></img></div>
    <img class='beaten-budge' title="Beaten" beaten='<?= isset($game->isBeaten) && $game->isBeaten ? 'true' : 'false' ?>' src="<?= base_url('images/budgeSmall.png') ?>"></img>
    <!-- Details -->
    <div class='details' >
      <div class='block platform'>
        <div class='title'>Platform :</div>
        <div class='value'><?= $game->Platform->Value->Name ?></div>
      </div>

      <div class='block genre'>
        <div class='title'>Genre :</div>
        <div class='value'>Not Available</div>
      </div>

      <div class='block releasedate'>
        <div class='title'>Release Date :</div>
        <div class='value'><?= $game->ReleaseDate->General->Value->format("d/m/Y") ?></div>
      </div>
    </div>
  </div>
</div>
<?php } ?>


<?php /* Javascript */ ?>

<script type="text/javascript">

$(document).ready(function(){

  // Slide Own Button
  $('.gameblock > .header').hover(function(){
    $(this).find(".own_status_full").stop().animate({"width":"70px"},250);
  },function(){
    if($(this).find(".own_status_full").attr("pending") == "false")
      $(this).find(".own_status_full").stop().animate({"width":"0px"},150);
  });

  // Beaten Budge Hover
  $('.gameblock').find('.beaten-budge').hover(function(){
    var beaten = $(this).attr("beaten");
    if(beaten == "pending") return;

    $(this).css("opacity",0.59);
  },function(){
    var beaten = $(this).attr("beaten");
    if(beaten == "pending") return;
    if(beaten == "true")
      $(this).css("opacity",1);
    else
      $(this).css("opacity",0.2);

  });

  // Beaten Budge Click Effect
  $('.gameblock').find('.beaten-budge').click(function(){

    if($(this).attr("beaten") == "pending") return;

    // Get Game Block Data
    var gameID =  $(this).parent().parent().attr("gameID");
    var beaten =  $(this).attr("beaten") == "true";
    console.log(gameID);
    console.log(beaten);

    var self = this;
    $(this).attr("beaten","pending");
    $(this).removeAttr('style');
    // Start The Effect
    var beaten_effect = window.setInterval(function(){
      $(self).animate({'right':'4px'},10,function(){
        $(this).animate({'right':'2px'},10)
      })
    },20);

    // Give some time to show the effect
    window.setTimeout(function(){
      // Ajax Request for setting game as beaten
    $.post('<?= site_url("game/beaten") ?>',{"gameID":gameID,"beaten":!beaten},
      function(data){
        data = JSON.parse(data);
        console.log(data['action']);
        window.clearInterval(beaten_effect);
        $(self).finish();
        $(self).css("right","3px");

        if(data.action == "success")
          beaten = !beaten;
        console.log(beaten);
        if(beaten)
          $(self).attr("beaten","true");
        else
          $(self).attr("beaten","false");
      });

    },200);


  });

  $(document).on('GameAddedToList',function(event,listID,gameID){
    var game = $(".gameblock[gameID="+gameID+"]").find(".own_status_full");
    $(game).animate({"width":"0px"},150,function(){
      $(game).text("Remove").attr("owner","true");
      $(game).parent().find(".own_status").attr("owner","true"); });
      $(game).attr("pending","false");
  });

  $(document).on('GameRemovedFromList',function(event,listID,gameID){
    var game = $(".gameblock[gameID="+gameID+"]").find(".own_status_full");
    $(game).animate({"width":"0px"},150,function(){
      $(game).text("I Own It").attr("owner","false");
      $(game).parent().find(".own_status").attr("owner","false");
      $(game).attr("pending","false");
    });
  });

});

function changeOwnStatus(game){

  var ajaxPending = $(game).attr("pending") == "true";
  if(ajaxPending) return;

  var gameID = $(game).parent().parent().attr("gameID");
  var owner = $(game).attr("owner") == "true";

  if(!owner) {
    $(game).attr("pending","true");
    $(game).html("<img src='<?= base_url('images/ajax-loader.gif')?>''></img> ");
    window.setTimeout(function(){ListManager.addGame(gameID,-1);},700);

  } else {
    $(game).attr("pending","true");
    $(game).html("<img src='<?= base_url('images/ajax-loader.gif')?>''></img> ");
    window.setTimeout(function(){ListManager.removeGame(gameID,-1);},700);
  }

}
</script>
