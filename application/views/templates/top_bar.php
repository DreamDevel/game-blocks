<?php 

// Get Web Site Name
$site_name = $this->config->item('website_name', 'tank_auth');

$user = $this->tank_auth->get_current_user();

// Fix For Deleted Users
if($this->tank_auth->is_logged_in() && is_null($user))
  $this->tank_auth->logout();

// Create mail hash for gravatar
if(isset($user))
  $userMailHash = md5( strtolower( trim( $user->email ) ) );
else
  $userMailHash = md5( strtolower( trim( "No_Mail" ) ) );

?>

<nav class="top-bar" data-topbar>
  <ul class="title-area">
    <li class="name">
      <h1><a href=<?= site_url('/') ?>> <img src=<?= base_url('images/logo.svg') ?> style="width:30px;"><?= $site_name ?></a></h1>
    </li>
  </ul>


  <section class="top-bar-section"> 
    <ul class="left">

      <li class="divider"></li>
      <li><a href=<?= site_url('games'); ?>>Games</a></li>
      <li><a href=<?= site_url('platforms'); ?>>Platforms</a></li>
    </ul>
    <ul class="right">

    <?php if(!$this->tank_auth->is_logged_in()) : ?>

      <li><a id="loginButton" href="#" data-reveal-id="LoginRegisterModal">Login or Register</a></li>
    <?php else : ?>

    <?php if($user->role_id == 2): ?>
      <li class="divider"></li>
      <li class="has-dropdown">
          <a href=<?= site_url('admin'); ?> >Admin Panel</a>
          <ul class="dropdown">
            <li><a href=<?= site_url('admin'); ?>>Home</a></li>
            <li><a href=<?= site_url('admin/usersList'); ?>>Users List</a></li>
            <li><a href=<?= site_url('admin/gamesList'); ?>>Games List</a></li>
            <li><a href=<?= site_url('admin/GamesDB'); ?>>Games DB</a></li>
            <li><a href=<?= site_url('admin/settings'); ?>>Site Settings</a></li>
          </ul>
      </li>
      <li class="divider"></li>
    <?php endif; ?>
    
    <li class="has-dropdown">
      
      <!-- Username And Avatar -->
      <a href="#">  
        <img src=<?= "http://www.gravatar.com/avatar/{$userMailHash}" ?> style="height:30px; width:30px; border:1px solid #ccc;margin-right:10px;"> 
        <?= ucfirst($user->username) ?>
      </a>

        <ul class="dropdown">
          <li><a href=<?= site_url("profile/myLists") ?> >My Lists</a></li>
          <li><a href=<?= site_url("account/settings")?> >Settings</a></li>
          <li><?= anchor('/auth/logout/', 'Logout') ?></li>
        </ul>

      </li>
    <?php endif; ?>

    </ul>
  </section>
</nav>