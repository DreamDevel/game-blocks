<?php $this->load->view('admin/base') ?>
<h4> Game Details </h4>
<ul>
        
 		<strong>ID:</strong> <?= $game->ID ?></br>

        <strong>Title:</strong> <?= $game->Title ?></br>
        <form action=<?= site_url("admin/changeGameTitle/{$game->ID}")?> method="post" >
             New Title: <input type="text" value=<?=  '"' . $game->Title . '"'?> name="newTitle">
             <button type="submit">Change Title</button>
        </form>

        <strong>Cover Image URL:</strong> <?= $game->CoverImageURL ?>
        <form action=<?= site_url("admin/changeCoverImageURL/{$game->ID}")?> method="post" >
             New Cover Image URL: <input type="text" value=<?=  '"' . $game->CoverImageURL . '"'?> name="newURL">
             <button type="submit">Change URL</button>
        </form>
      
 		<h5><strong>Publisher</strong></h5>
 		<ul>
 		    <li><strong>ID: </strong> <?= $game->Publisher->ID ?></li>
            <li><strong>Name: </strong> <?= $game->Publisher->Name ?></li>

              <!--Publisher dropdown -->
            <a href="#" data-dropdown="publisherDrop" class="button dropdown"><?= $game->Publisher->Name?></a><br>
            <ul id="publisherDrop" data-dropdown-content class="f-dropdown">
                 <?php foreach($publishers as $row) : ?>
                    <li><a href="<?= site_url("admin/changeGamePublisher/{$game->ID}/{$row->ID}")?>"  ><?= $row->Name?></a></li>
                 <?php endforeach; ?>
            </ul>

			<li><strong>Region:</strong> <?= $game->Publisher->Region ?></li>
 		</ul>

 		<h5><strong>Platform</strong></h5>
 		<ul>
            <li><strong>ID: </strong> <?= $game->Platform->ID ?></li>
    		<li><strong>Name:</strong> <?= $game->Platform->Name ?></br></li>
            <!--platform dropdown -->
            <a href="#" data-dropdown="platformDrop" class="button dropdown"><?= $game->Platform->Name?></a><br>
            <ul id="platformDrop" data-dropdown-content class="f-dropdown">
                 <?php foreach($platforms as $row) : ?>
                    <li><a href="<?= site_url("admin/changeGamePlatform/{$game->ID}/{$row->ID}")?>"  ><?= $row->Name?></a></li>

                 <?php endforeach; ?>
            </ul>

    		<li><strong>Manufacturer :</strong> <?= $game->Platform->Manufacturer ?></li>
 		</ul>

        <h5><strong>Developer</strong></h5>
        <ul>
            <li><strong>ID: </strong> <?= $game->Developer->ID ?></li>
            <li><strong>Name:</strong> <?= $game->Developer->Name ?></br></li>
            
            <!-- Developer dropdown-->
            <a href="#" data-dropdown="developerDrop" class="button dropdown"><?= $game->Developer->Name?></a><br>
            <ul id="developerDrop" data-dropdown-content class="f-dropdown">
                 <?php foreach($developers as $row) : ?>
                    <li><a href="<?= site_url("admin/changeGameDeveloper/{$game->ID}/{$row->ID}")?>"  ><?= $row->Name?></a></li>

                 <?php endforeach; ?>
            </ul>
        </ul>

 </ul>