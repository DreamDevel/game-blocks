<br>
<form action="<?=site_url("/admin/addGame")?>" method="post">

  <div class="row">

    <div class="large-6 columns">
      <label>TiTle</label>
      <input type="text" name="title" placeholder="game title" required/>
    </div>

    <div class="large-6 columns">
      <label>Image Url</label>
      <input type="text" name="url" placeholder="Enter your url here..." required/>
    </div>

  </div>

  <div class="row">

    <div class="large-4 columns">
      <label>Select Platform</label>
      <select name="platform">
        <?php foreach($platforms as $platform) : ?>
             <option value="<?= $platform->ID ?>"><?= $platform->Name?></option>
        <?php endforeach; ?>
        <option value="other">Other</option>
      </select>
    </div>

    <div class="large-4 columns">
      <label>Select Developer</label>
      <select name="developer">
        <?php foreach($developers as $developer) : ?>
             <option value="<?= $developer->ID ?>"><?= $developer->Name?></option>
        <?php endforeach; ?>
        <option value="other">Other</option>
      </select>
    </div>

    <div class="large-4 columns">
      <label>Select Publisher</label>
      <select name="publisher">
        <?php foreach($publishers as $publisher) : ?>
             <option value="<?= $publisher->ID ?>"><?= $publisher->Name?></option>
        <?php endforeach; ?>
        <option value="other">Other</option>
      </select>
    </div>


  </div>

  <div class="row">

    <div class="large-6 columns">
      <label>Online</label>
      <input type="radio" name="online" required value="Yes" id="ADonlineYes"><label for="ADonlineYes">Yes</label>
      <input type="radio" name="online" value="No"  id="ADonlineNO"><label for="ADonlineNO">No</label>
      <input type="radio" name="online" value="Null"  id="ADonlineDN"><label for="ADonlineDN">Dont Know</label>
    </div>

    <div class="large-6 columns">
      <label>Split Screen</label>
      <input type="radio" name="splitScreen" value="Yes" id="ADsplitScreenYes"><label for="ADsplitScreenYes">Yes</label>
      <input type="radio" name="splitScreen" value="No"  id="ADsplitScreenNO"> <label for="ADsplitScreenNO">No</label>
      <input type="radio" name="splitScreen" value="No"  id="ADsplitScreenDN"> <label for="ADsplitScreenDN">Dont Know</label>
    </div>

   </div>

  <div class="row">
    <div class="small-12 columns">
      <label>Description</label>
      <textarea name="description" placeholder="Decription" required></textarea>
    </div>
  </div>

  <button type="submit" style="margin-top:20px; margin-left:200px;" class="button radius small">Submit</button>
</form>
