<?php $this->load->view('admin/base') ?>

<a href=<?=site_url("/admin/formToAddGame")?> class="right" style="position:absolute; right:30px; top:70px;">Add New Game</a>

<?php if($games!=NULL):?>
  <table style="margin: 80px auto;">
   <thead>
     <tr>
       <th>Link</th>
    
       <?php if($this->uri->segment(3)){

               if($this->uri->segment(4)=='a') 
                  $method = 'd';
               else 
                  $method = 'a';
             }
             else 
                $method = 'd';
        ?>

        <th><a href=<?=site_url("/admin/gamesList/byID/{$method}")?>>ID</a></th>
        <th><a href=<?=site_url("/admin/gamesList/byTitle/{$method}")?>>Title</a></th>
        <th><a href=<?=site_url("/admin/gamesList/byPlatform/{$method}")?>>Platform</a></th>
        
     </tr>

   </thead>

   <tbody>
    <?php foreach($games as $game) : ?>

	    <!--columns -->
		 <tr>
      
      <td> <a href= <?= site_url("admin/game/{$game->ID}");?> ><img src=<?= base_url("images/admin/view.png")?> /></a> </td>
			<td> <?= $game->ID ?> </td>
			<td> <?= $game->Title ?> </td>
			<td> <?= $game->Platform->Name ?> </td>
	
		 </tr>
      
    <?php endforeach; ?>
   </tbody>
  </table>
<?php endif;?>