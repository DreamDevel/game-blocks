<div class='platformsGamesDB'>

<button class='tiny' style='margin:10px;' onclick="location.href='<?= site_url('admin/GamesDB/') ?>'">Back To GamesDB</button>

<div class='controls'>
	<button class='tiny' onclick="$('#updating').show();location.href='<?= site_url('admin/GamesDB/Platforms/update/') ?>'">Update</button>
	<button class='tiny' onclick="$('#updateCheck').show();location.href='<?= site_url('admin/GamesDB/Platforms/checkForUpdate/') ?>'">Check For Updates</button>
	<button class='tiny' onclick="$('#updateCheck').show();location.href='<?= site_url('admin/GamesDB/Platforms/LinkAll/') ?>'">Link All Platforms</button>
</div>

<div style='text-align:center'>
	<div class='infoBox'>
		<div class='title'>
			Platforms In Database
		</div>
		<div class='value'>
			<?= !$platforms ? 0 : count($platforms) ?>
		</div>
	</div>
	<div class='infoBox'>
		<div class='title'>
			Unlinked Platforms
		</div>
		<div class='value'>
			<?= $unlinkedPlatforms ?>
		</div>
	</div>
</div>

<div class='info'>

	<?php if(isset($updatesCount)) : ?>

  	<div id='newPlatformsFound' data-alert class="alert-box success">
  	<?= $updatesCount ?> new <?= $updatesCount > 1 ? 'platforms' : 'platform' ?> found.
  	<a href="#" class="close">&times;</a>
  	</div>

	<?php endif; ?>

	<?php if(isset($platformsUpdated)) : ?>

	<div id='newPlatformsAdded' data-alert class="alert-box success">
	<?= $platformsUpdated ?> new <?= $platformsUpdated > 1 ? 'platforms' : 'platform' ?> added.
	<a href="#" class="close">&times;</a>
	</div>

		<?php endif; ?>

	<div id='updating' data-alert class="alert-box warning">
		<img src="<?= base_url('images/ajax-loader.gif') ?>" style='margin-right:20px;'></img> Updating, Please Wait!
	<a href="#" class="close">&times;</a>
	</div>

	<div id='updateCheck' data-alert class="alert-box warning">
		<img src="<?= base_url('images/ajax-loader.gif') ?>" style='margin-right:20px;'></img> Checking for updates, Please Wait!
	<a href="#" class="close">&times;</a>
	</div>

</div>

<?php if(!$platforms) : ?>

	<div data-alert class="alert-box warning" style='width:500px;margin:20px auto;'>
  There are no platforms in database. Please Update.
  <a href="#" class="close">&times;</a>
</div>
<?php else : ?>

	<div class='platformsWrapper'>
	<?php foreach ($platforms as $platform) : ?>
		<div class='platform'>
		<div class='id'>
			<?= $platform->getID() ?> 
		</div>
		<div class='title'> 
			<?= $platform->getPlatform() ?> 
		</div>
		</div>
	<?php endforeach; ?>
	</div>
<?php endif ?>
</div>
<div>

</div>

<script>
$('.platform').on('mouseenter',function(){
    $(this).animate({height:'90px',marginTop: '0px'},200);
    $(this).find('.id').css({display:'inline'});
});

$('.platform').on('mouseleave',function(){
    $(this).animate({height:'80px',marginTop: '10px'},200);
    $(this).find('.id').css({display:'none'});
});
</script>