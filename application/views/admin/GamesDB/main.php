
<h1 style="color:white;margin-top:50px;text-align:center;"><u>Games Database Controls</u></h1>

<div class='mainGamesDB'>
	<div class='box' onClick="window.location.href='<?= site_url('/admin/GamesDB/Platforms') ?>'">
		<img src="<?= base_url('images/admin/consolegdb.png') ?>"></img>
		<div class='title'>Platforms</div>
	</div>


	<div class='box' onClick="window.location.href='<?= site_url('/admin/GamesDB/Games') ?>'">
		<img src="<?= base_url('images/admin/gamesgdb.png') ?>"></img>
		<div class='title'>Games</div>
	</div>


	<div class='box' onClick="window.location.href='<?= site_url('/admin/GamesDB/Settings') ?>'">
		<img src="<?= base_url('images/admin/settingsgdb.png') ?>"></img>
		<div class='title'>Settings</div>
	</div>
</div>