

<div  class='gamesGamesDB' style='color:white'>
<button class='tiny' style='margin:10px;' onclick="location.href='<?= site_url('admin/GamesDB/') ?>'">Back To GamesDB</button>

<div style='text-align:right;margin-right:120px;font-size:14px'>
Updating: <img id='updatingImgGreen' src= <?= '"' . base_url('images/greenCircle.png') . '"'?> style='width:20px;height:20px;'> <img id='updatingImgRed' src= <?= '"' . base_url('images/redCircle.png') . '"'?> style='width:20px;height:20px;'>
</div>

<div style='text-align:center;margin:20px 0;'>

	<div>
		<button class='tiny' onclick="location.href='<?= site_url('admin/GamesDB/Games/Update') ?>'">Update All Games</button> 
		<button class='tiny' onclick="location.href='<?= site_url('admin/GamesDB/Games/UpdateCheck') ?>'">Check For Updates</button>
		<button class='tiny' onclick="location.href='<?= site_url('admin/GamesDB/Games/LinkAll') ?>'">Link All Games</button>
	</div>

	<div id='dbGames' class='infoBox'>
		<div class='title'>
			Games In Database
		</div>
		<div class='value'>
			<?= $dbGames ?>
		</div>
	</div>
	
	<div id='missingGames' class='infoBox'>
		<div class='title'>
			Missing Games
		</div>
		<div class='value'>
			<?= $missingGames ?>
		</div>
	</div>

	<div id='linkedGames' class='infoBox'>
		<div class='title'>
			Linked Games
		</div>
		<div class='value'>
			<?= $linkedGames ?>
		</div>
	</div>

	<div id='unlinkedGames' class='infoBox'>
		<div class='title'>
			Unlinked Games
		</div>
		<div class='value'>
			<?= $dbGames - $linkedGames ?>
		</div>
	</div>

	<br>
	<div class='infoBox'>
		<div class='title'>
			Last Check
		</div>
		<div class='value'>
			<?= $lastUpdateCheck ? $lastUpdateCheck->format("d-M-Y H:i") : 'Never' ?>
		</div>
	</div>

	<div class='infoBox'>
		<div class='title'>
			Last Update
		</div>
		<div class='value'>
			Not Available Yet
		</div>
	</div>
</div>

<div class='platformsWrapper'>
<?php if($platforms) : ?>
<?php foreach($platforms as $platform): ?>
	<div class='platform'>
		<div class='id'>
			<?= $platform->getID() ?>
		</div>
		<div class='title'>
			<?= $platform->getPlatform() ?>
		</div>

		<div class='dbGames'>
			<div class='title'>In Database</div>
			<div class='gamesNumber'>
				<?= $platform->numberOfLocalGames ?>
			</div>
		</div>
		<div class='newGames'>
			<div class='title'>New</div>
			<div class='gamesNumber' <?= $platform->numberOfNewGames > 0 ? "style='background-color:#BDBDBD;color:black'" : ""  ?> > 
				<?= $platform->numberOfNewGames ?>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<?php else : ?>
	<div style="color:#FF2E2E">No Platforms Available! Please update platforms first</div>
<?php endif; ?>
</div>

<script>


$(document).ready(function(){

	missingGames = <?= $missingGames ?>;
	updating = <?= (integer) $updating ?>;

	if(updating && missingGames > 0)
		$('#updatingImgRed').hide(0);
	else
		$('#updatingImgGreen').hide(0);

	if(missingGames > 0 && updating ){

		var updateMissingGames = function(){

			$.get(<?= '"' . current_url() . '/Async-MissingGames' . '"' ?>,function(data){

				console.log(data);
				$('#missingGames .value').text(data);

				if(data > 0){
					window.setTimeout(updateMissingGames,2000)
				}
				else{
					$('#updatingImgGreen').hide(0);
					$('#updatingImgRed').show(0);
				} 

			});

			$.get(<?= '"' . current_url() . '/Async-GamesInDB' . '"' ?>,function(data){

				console.log(data);
				$('#dbGames .value').text(data);

			});

			$('#unlinkedGames .value').text( $('#dbGames .value').text() - $('#linkedGames .value').text() );


		}
		// Start
		updateMissingGames();
	}
});


</script>
