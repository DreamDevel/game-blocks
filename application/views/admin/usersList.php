<?php $this->load->view('admin/base') ?>

    <form method="get" action="<?= site_url("admin/searchBar")?>" >
            <input type="text" name="item" size="21" maxlength="120">
            <button type="submit">Search</button>
    </form>

<h3 style="text-align:center">Administrator Page</h3>

<table style="margin:0 auto;">
   <thead>
     <tr>
        <th>Link</th>
        <th>ID</th>
        <th>Username</th>
        <th>Email</th>
        <th>Activated</th>
        <th>Banned</th>
        <th>Last Login</th>
        
     </tr>

   </thead>
   <tbody>
   <?php foreach($users as $user) : ?>

	    <!--columns -->
		<tr>
		  <td> <a href= <?= site_url("admin/user/{$user->id}");?> ><img src=<?= base_url("images/admin/view.png")?> /></a> </td>
			<td> <?= $user->id ?> </td>
			<td> <?= $user->username ?> </td>
			<td> <?= $user->email ?> </td>
			<td> <?= $activated = ($user->activated == 0 ) ? "No" : "Yes"; ?> </td>
			<td> <?= $banned = ($user->banned == 0) ? "No" : "Yes"; ?> </td>
			<td> <?= $user->last_login ?> </td>
			
			

		</tr>
      
   <?php endforeach; ?>
   </tbody>
</table>