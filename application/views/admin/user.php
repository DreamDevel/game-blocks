<?php
// Create mail hash for gravatar
if(isset($user))
  $userMailHash = md5( strtolower( trim( $user->email ) ) );
else
  $userMailHash = md5( strtolower( trim( "No_Mail" ) ) );

?>

<?php $this->load->view('admin/base') ?>


<div style="width:700px;margin:30px auto;border:1px solid #ccc">

  <div class="right" style="border:1px solid #BDBDBD;margin:10px;width:230px;box-shadow: 1px 1px 1px #888888;">
  <?php if (!$user->activated) : ?>
  <div class="row">
    <div><div style="display:inline-block;width:130px;font-weight:bold">Account Status:</div>
    <div style="display:inline-block;font-weight:bold;color:#BF0000">Inactive</div>
    </div>
  </div>
  <?php else : ?>
  <div class="row">
  <div><div style="display:inline-block;width:130px;font-weight:bold">Account Status:</div>
    <div style="display:inline-block;font-weight:bold;color:#00BF10">Activated</div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!$user->banned) : ?>
  <div class="row">
  <div><div style="display:inline-block;width:130px;font-weight:bold">Ban Status:</div>
    <div style="display:inline-block;font-weight:bold;color:#00BF10">Not Banned</div>
  </div>
  </div>
  <?php else : ?>
  <div class="row">
  <div><div style="display:inline-block;width:130px;font-weight:bold">Ban Status:</div>
    <div style="display:inline-block;font-weight:bold;color:#BF0000">Banned</div>
  </div>  
  </div>
  <?php endif; ?>
  </div>

<div style="margin:10px">
      <img src=<?= "http://www.gravatar.com/avatar/{$userMailHash}" ?> style="height:75px; width:75px; border:1px solid #ccc;margin-right:10px; float:left;"> 
      <div style="font-weight:bold;font-size:22px;"> User <?= $user->username ?>       </div>
        <div>
        <a href="#" data-dropdown="drop" class="button dropdown  tiny" style="margin-top:15px;"><?= $role->name?></a><!--
     --><a href="#" data-reveal-id="deleteUserModal" class="button tiny alert <?= $user->role_id ==2 ? 'disabled' : '' ?>">Delete </a> 
        <?php if($user->banned) : ?>
        <a href=<?= site_url("admin/unbanUser/{$user->id}") ?> class="button tiny alert <?= $user->role_id ==2 ? 'disabled' : '' ?>">Un-Ban </a>
        <?php else : ?>
        <a href="#" data-reveal-id="banUserModal" class="button tiny alert <?= $user->role_id ==2 ? 'disabled' : '' ?>">Ban </a>
        <?php endif; ?>
        <?php if(!$user->activated) : ?>
        <a href=<?= site_url("admin/activateUser/{$user->id}") ?> class="button tiny alert">Activate</a>
        <?php endif; ?>
        <br>
        <ul id="drop" data-dropdown-content class="f-dropdown">
           <?php foreach($roles as $row) : ?>
              <li><a href= " <?= site_url("admin/changeUserRole/{$user->id}/{$row->id}")?>"  ><?= $row->name?></a></li>
           <?php endforeach; ?>
        </ul>
        </div>
</div>
<hr />

<?php if($user->banned) : ?>
<div data-alert class="alert-box alert">
  <strong>Ban Reason:</strong> <?= $user->ban_reason?>
</div>
<?php endif; ?>

<div class="row" style="margin:5px 5px;">
  <div style="display:inline-block;width:100px;">ID:</div>
  <div style="display:inline-block;width:250px;"><input type="text" value=<?= $user->id ?> class="inputNoStyle" readonly></div>
</div>

<div class="row" style="margin:5px 5px;">
  <div style="display:inline-block;width:100px;">Username:</div>
  <div style="display:inline-block;width:250px;"><input type="text" value=<?= $user->username ?> class="inputNoStyle"></div>
</div>

<div class="row" style="margin:5px 5px;">
  <div style="display:inline-block;width:100px;">Password:</div>
  <div style="display:inline-block;width:250px;"><input type="password" placeholder="Type a new password" class="inputNoStyle"></div>
</div>

<div class="row" style="margin:5px 5px;">
  <div style="display:inline-block;width:100px;">Email:</div>
  <div style="display:inline-block;width:250px;"><input type="text" value=<?= $user->email ?> class="inputNoStyle"></div>
</div>

<div class="row" style="margin:5px 5px;">
  <div style="display:inline-block;width:100px;">Last IP:</div>
  <div style="display:inline-block;width:250px;"><input type="text" value=<?= $user->last_ip?> class="inputNoStyle" readonly></div>
</div>


<div class="row" style="margin:5px 5px;">
  <div style="display:inline-block;width:100px;">Last Login:</div>
  <div style="display:inline-block;width:250px;"><input type="text" value=<?= $user->last_login ?> class="inputNoStyle" readonly></div>
</div>

<div class="row" style="margin:5px 5px;">
  <div style="display:inline-block;width:100px;">Registered:</div>
  <div style="display:inline-block;width:250px;"><input type="text" value=<?= $user->created?> class="inputNoStyle" readonly></div>
</div>

<div class="row" style="margin:5px 5px;">
  <div style="display:inline-block;width:100px;">Modified:</div>
  <div style="display:inline-block;width:250px;"><input type="text" value=<?= $user->modified?> class="inputNoStyle" readonly></div>
</div>

</div>

<div id="banUserModal" class="reveal-modal small" data-reveal>
  <h2><img src="http://icons.iconarchive.com/icons/kyo-tux/soft/128/Alert-icon.png" style="float:left; width:50px; margin-right:15px;" /> Ban User</h2>
  <p class="lead">Please enter a ban reason</p>
  <form data-abide action=<?= site_url("admin/banUser/{$user->id}") ?> method="post">
    <div class="name-field">
      <label>Ban Reason</label>
      <input name="ban_reason" type="text" placeholder="User got banned because...">
      <small class="error">Ban reason is required</small>
    </div>
    <button type="submit" class="alert small right">Ban User</button>
  </form>
  <a class="close-reveal-modal">&#215;</a>
</div>


<div id="deleteUserModal" class="reveal-modal small" data-reveal>
  <h2><img src="http://icons.iconarchive.com/icons/kyo-tux/soft/128/Alert-icon.png" style="float:left; width:50px; margin-right:15px;" /> Delete User</h2>
  <p class="lead" style="color:red">Are you sure you want to delete this user ? This action cannot be undone!</p>
  <a href=<?= site_url("admin/deleteUser/{$user->id}") ?> class="button">Yes</a> <a href="#" onClick="$('#deleteUserModal').foundation('reveal', 'close', '');" class="button">Cancel</a>
  <a class="close-reveal-modal">&#215;</a>
</div>
        
<?php
/*
              //change user's password button
             echo form_open("admin/changeUserPassword/{$user->id}");
             echo "<strong>If you want to change this user's password, first enter a new password</strong></br>";
             echo "</br>New Password: ".form_password('new_password');
             echo "<button type='submit'>Change Password</button>";
             echo form_close();
*/
?>
       

        
       
