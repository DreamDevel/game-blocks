<?php

// Calculate The Image Size TODO

// Get Platform CSS TODO

$cssPath	= base_url() . 'css/';
$imagesPath = base_url() . 'images/';

$coverImageURL = $game->CoverImage->Value;

// Set The Custom Background Based On Console
$PlatformTrimmed = strtolower(preg_replace('/\s/', '', $game->Platform->Value->Name));

//$user = $this->tank_auth->get_current_user();

# Check If Developer Publisher And Release Date Exist

if($game->Developer && $game->Developer->DataProvider == 'GB')
	$Developer = $game->Developer->Value->Name;
elseif($game->Developer && $game->Developer->DataProvider == 'GDB')
	$Developer = $game->Developer->Value;
else
	$Developer = 'Unknown';

if($game->Publisher && $game->Publisher->DataProvider == 'GB')
	$Publisher = $game->Publisher->Value->Name;
elseif($game->Publisher && $game->Publisher->DataProvider == 'GDB')
	$Publisher = $game->Publisher->Value;
else
	$Publisher = 'Unknown';	

//if($game->ReleaseDateUSA)
	$ReleaseDateUSA = 'Unknown';
/*else {

	$phpDate = strtotime($game->ReleaseDateUSA);
	$ReleaseDateUSA = date( 'd-m-Y', $phpDate );
}	*/
?>

<div class=<?= '"c' . $PlatformTrimmed . ' fixedBackground"' ?>> </div>
<div id='GameView' class=<?= '"c' . $PlatformTrimmed . ' view"' ?>>
<!-- <div class="devider"></div> -->
	<div class="head" style="position:relative;">
		<div class="title">
			<?= $game->Title->Value ?>
			<?php if($this->tank_auth->is_logged_in() ) : ?>
			<span data-tooltip  title="Game is already in collection, Click to remove it">
				<img class="removeGameBtn" onclick=<?= "ListManager.removeGame({$game->ID},-1);"?> style="display:inline-block;margin-left:5px;width:24px;height:24px;vertical-align:middle;" src="http://icons.iconarchive.com/icons/gakuseisean/ivista-2/24/Alarm-Tick-icon.png" />
			</span>
			<span data-tooltip  title="Game is not in collection, Click to add it">
				<img class="addGameBtn" onclick=<?= "ListManager.addGame({$game->ID},-1);"?> style="display:inline-block;margin-left:5px;width:24px;height:24px;vertical-align:middle;" src="http://icons.iconarchive.com/icons/gakuseisean/ivista-2/24/Alarm-Error-icon.png" />
			</span>
			<?php endif; ?>
		</div>
		<div class="toolbox" style="width:400px;position:absolute;right:0;bottom:0;" style="display:inline-block;">
				<a href="javascript:void(0)" onClick='$.post("http://localhost/game-blocks/index.php/game/beaten",{"gameID":<?= $game->ID ?>,"beaten":true},function(data){console.log(data);});' >Set As Beaten<a/> - <a href="javascript:void(0)" onClick='$.post("http://localhost/game-blocks/index.php/game/beaten",{"gameID":<?= $game->ID ?>,"beaten":false},function(data){console.log(data);});'>Set As Not Beaten</a>
				<!-- Toolbox Items
				<div style="display:inline-block;background-color:green;width:30px;height:30px;vertical-align:middle;"></div>
				-->
		</div>
	</div>
	<div class="topContainer" style="margin-bottom:2px;">
		<div class="coverImage">
			<img src=<?= $coverImageURL ?> />
		</div><!--

		--><div class="generalInfo">
			<div class="categorylabel">General Info</div>
			<div class="info" style="">
			<ul>
			<li>Platform:     <a href="#"> <?= $game->Platform->Value->Name ?>  </a></li>
			<li>Developer:    <a href="#"> <?= $Developer ?> </a></li>
			<li>Publisher:    <a href="#"> <?= $Publisher ?> </a></li>
			<li>Release Date: <a href="#"> <?= $ReleaseDateUSA ?>        </a></li>
			</ul>

			</div>

		</div>
	</div>

	<div class="description" style="margin-bottom:2px;">
		<div class="categorylabel">Description</div>
		<div>
			<?= $game->Description ? $game->Description->Value : "No description available" ?>
		</div>
	</div>

<div class="multiplayer">
		<div class="multiplayerLeft" style="margin-bottom:2px;">
		<div class="categorylabel center">Online</div>
		<div>
			<ul>
			<li>Online:     <a href="#"> Yes  </a></li>
			<li>Online Co-Op:    <a href="#"> No </a></li>
			<li>Online Players:    <a href="#"> 6 </a></li>
			</ul>
		</div>
	</div><!--
 --><div class="multiplayerCenter" style="margin-bottom:2px;">
		<div class="categorylabel center">Split Screen</div>
		<div>
			<ul>
			<li>Split Screen:     <a href="#"> Yes  </a></li>
			<li>Split Screen Co-Op:    <a href="#"> No </a></li>
			<li>Split Screen Players:    <a href="#"> 6 </a></li>
			</ul>
			
		</div>
	</div><!--
 --><div class="multiplayerRight" style="margin-bottom:2px;">
		<div class="categorylabel center">Lan Play</div>
		<div>
			<ul>
			<li>Split Screen:     <a href="#"> Yes  </a></li>
			<li>Split Screen Co-Op:    <a href="#"> No </a></li>
			<li>Split Screen Players:    <a href="#"> 6 </a></li>
			</ul>
			
		</div>
	</div>
</div>
	<div class="foot"></div>
</div>

	<div id="loadIndicator">
		<img src= <?= base_url('images/ajax-loader.gif') ?> /> <br>
		Loading
	</div>

<script src=<?= base_url('/js/ListManager.js') ?> ></script>
<script src=<?= base_url('/js/Notifier.js')    ?> ></script>
<script src=<?= base_url('/js/gameview.js')    ?> ></script>
<script>
$(document).ready(function(){

	var game = <?= json_encode($game) ?>;
	game.inCollection = <?= $gameInCollection ? 'true' : 'false' ?>;
	var siteurl = <?= '"' . site_url()  . '"' ?>;
	
	gameView.initialize(true,game,siteurl);
});
</script>