
<center>
<?php

echo "<h3 style='color:white;'>The Blocks Of Your Gaming Life</h3>";
?>
<div class="home"></div>
<!--
<div style="border-top:1px solid #545353;border-left:1px solid #545353;border-right:1px solid #545353;
border-bottom:1px solid #3D3D3D;
width:700px;height:30px;background-color:#4A4A4A;
-webkit-border-top-left-radius: 10px;-webkit-border-top-right-radius: 10px;"></div>
<div style="width:700px;">
<ul class="example-orbit" data-orbit>
  <li>
  	<?= "<a href='" . site_url('game/index/1') . "'>" ?>
    <img src="http://images.nintendolife.com/news/2013/08/the_legend_of_zelda_the_wind_waker_hd_confirmed_for_4th_october_in_north_america/attachment/0/original.jpg" alt="slide 1" />
    <div class="orbit-caption">
      The Legend Of Zelda Wind Waker (Wii U)
    </div>
    </a>
  </li>
  <li>
    <?= "<a href='" . site_url('game/index/2') . "'>" ?>
    <img src="http://www.negativeworld.org/images/content/440/Super_Mario_3D_World_Thread_Banner.jpg" alt="slide 2" />
    <div class="orbit-caption">
      Super Mario 3D World (Wii U)
    </div>
    </a>
  </li>
  <li>
    <?= "<a href='" . site_url('game/index/3') . "'>" ?>
    <img src="http://images.vg247.com/current//2013/01/luigis-mansion-banner1.jpg" alt="slide 3" style="width:700px;height:212px;"/>
    <div class="orbit-caption">
      Luigi's Mansion (Gamecube)
    </div>
    </a>
  </li>
  <li>
    <?= "<a href='" . site_url('game/index/4') . "'>" ?>
    <img src="http://gamingsifu.com/wp-content/uploads/2013/05/Second-Son.png" alt="slide 3" style="width:700px;height:212px;"/>
    <div class="orbit-caption">
      inFAMOUS: Second Son (Playstation 4)
    </div>
    </a>
  </li>
    <li>
    <?= "<a href='" . site_url('game/index/5') . "'>" ?>
    <img src="http://whatsyourtagblog.files.wordpress.com/2013/10/pokemon-x-and-y-banner.jpg" alt="slide 3" style="width:700px;height:212px;"/>
    <div class="orbit-caption">
      Pokemon X (Nintendo 3DS)
    </div>
    </a>
  </li>
</ul>
</div>
<div style="border-bottom:1px solid #545353;border-left:1px solid #545353;border-right:1px solid #545353;
border-top:1px solid #3D3D3D;
width:700px;height:30px;background-color:#4A4A4A;
-webkit-border-bottom-left-radius: 10px;-webkit-border-bottom-right-radius: 10px;"></div>
</center>
-->

<!-- Latest Games -->
<div style="width:100%;margin-top:30px;overflow:hidden;white-space:nowrap;">
<div style="border-top:1px solid #545353;border-left:1px solid #545353;border-right:1px solid #545353;
border-bottom:1px solid #3D3D3D;
width:100%;height:30px;line-height:30px;background-color:#4A4A4A;color:white;
-webkit-border-top-left-radius: 10px;-webkit-border-top-right-radius: 10px;text-align:center;">Latest Additions</div>

  <div style="height:160px;line-height:160px;width:100%;text-align:center;background-color:#919191;">
<?php  foreach ($lastGames as $game) : ?>
<a href="<?= site_url('game/index/' . $game->ID) ?>">
  <img src="<?= $game->CoverImage ? $game->CoverImage->Value : 'No-Image'  ?>" style="max-width:100px;max-height:140px;margin-left:8px;margin-right:8px;">
</a>
 <?php endforeach; ?>
  </div>

  <div style="border-bottom:1px solid #545353;border-left:1px solid #545353;border-right:1px solid #545353;
border-top:1px solid #3D3D3D;
width:100%;height:30px;background-color:#4A4A4A;
-webkit-border-bottom-left-radius: 10px;-webkit-border-bottom-right-radius: 10px;"></div>
 </div>