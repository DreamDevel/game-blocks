<?php $this->load->view("templates/gameblock"); ?>

<!-- Search Area -->
<div style="width:80%; margin-top:50px; margin-left:auto; margin-right:auto;">
 <form id='searchForm' method="get" action="<?= site_url("games/index")?>" >
       <div style="font-size:22px; color:white; margin-bottom:10px; text-align:center;">Search for a game</div>
       <input id='gameInput' class="left inline" type="text" name="title" size="21" maxlength="120" style="width:92%; margin-right:3px; height:35px;">
       <input id='pageInput' name="p" type="hidden" value="1" >
       <button class="tiny" type="submit">Search</button>
 </form>
 </div>

<!-- Search Results -->
<?php if($games!=null):?>
  <div style="width:95%;margin-left:auto;margin-right:auto;text-align:center;">

    <!-- Games -->
    <?php foreach($games as $game) {
      echo_gameBlock($game,$inCollection[$game->ID]);
    } ?>

    <!-- Pages -->
    <div style='margin: 15px 0;color:white;'>
    <?php  for($i=0; $i<$numberOfPages; $i++) : ?>
      <?php if($_GET['p'] != $i+1) : ?>
        <a href='javascript:void(0)' onClick="changePage(<?= ($i+1) ?>)" > <?= ($i+1) ?> </a>
      <?php else : ?>
        <?= ($i+1) ?>
      <?php endif; ?>

      <?= $i == $numberOfPages-1 ? '' : " • " ?>
    <?php endfor; ?>
    </div>

  </div>
<?php endif; ?>

<!-- Initialize View -->
<script>
var gamesInCollection = <?= isset($inCollection) ? json_encode($inCollection) : 'null' ?>;
$(document).ready(function(){
    var siteurl = <?= '"' . site_url()  . '"' ?>;
    gamesView.initialize(true,gamesInCollection,siteurl);
});
</script>

<!-- Change Page Script -->
<script>
function changePage(page) {

  $('#gameInput').attr('value',"<?= $searchInput ?>");
  $('#pageInput').attr('value',page);
  $('#searchForm').submit();

}
</script>

<script src=<?= base_url('/js/ListManager.js') ?> ></script>
<script src=<?= base_url('/js/Notifier.js')    ?> ></script>
<script src=<?= base_url('/js/gamesview.js')    ?> ></script>
