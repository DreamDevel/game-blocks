<div class="register-form" style="width:523px; margin:15px auto; padding:10px;border:1px solid #ccc; background-color:#EBEBEB;opacity:0;">
	<h3 style="text-align:center;">Register for a free account</h3>
	<hr />
 	<form class="custom" action=<?= site_url("auth/register") ?> method="post" data-abide >
  			
      <!-- Username Input -->
        <div class="name-field" style="width:500px;">
  			<label>Username</label>
  			<input name="username" type="text" placeholder="Your Username" value=<?= "'" . set_value('username') . "'" ?> required>
          	<small class="error">
          		Username is required"
          	</small>
  		</div>

        <!-- Password Input -->
  		<div class="password-field" style="width:500px;">
  			<label>Password</label>
  			<input name="password" type="password" placeholder="Your Password" pattern="" required>
          	<small class="error">
          		Password is required. It must also be at least 6 characters long
          	</small>
  		</div>

         <!-- Password Input -->
  		<div class="password-field" style="width:500px;">
  			<label>Confirm Password</label>
  			<input name="confirm_password" type="password" placeholder="Your Password Again" pattern="" required>
          	<small class="error">
          		Confirm Password is required. Make sure it is the same as your password
          	</small>
  		</div>

  		<div class="email-field" style="width:500px;">
    		<label>Email</label>
    		<input name="email" type="email" placeholder="Your Email Address" value=<?= "'" . set_value('email') . "'" ?> required>
    		<small class="error">
    			An email address is required.
    		</small>
  		</div>

        <?php if ($captcha_registration && $use_recaptcha) : ?>
        		<hr />
        <div id="recaptcha_widget" style="display:none">
          <table>
            <tr>
              <td>
                <div id="recaptcha_image" style="width:300px;height:57px;overflow:hidden;"></div><br />
              </td>
              <td>
                <div><a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a></div>
                <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
                <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
              </td>
           </tr> 
          </table>

        	<div class="alpha_numeric-field" style="width:500px;">
        		<label><div class="recaptcha_only_if_image" >Enter the words above</div><div class="recaptcha_only_if_audio">Enter the numbers you hear</div></label>
        		<input type="text" id="recaptcha_response_field" placeholder="Captcha Code" name="recaptcha_response_field" required >
        		<small class="error">
          			Captcha code is required
        		</small>
        	</div>
      </div>
      	<hr />
    <?php endif; ?>
          
        <?php if(isset($errors['username']) || isset($errors['email']) || 
        form_error('confirm_password',' ', ' ') != false || form_error('recaptcha_response_field',' ', ' ') != false ||
        form_error('email',' ', ' ') != false) { ?>
        <div data-alert class="alert-box warning round text-center">
          <?php echo isset($errors['username'])?$errors['username']:''; ?>
          <?php echo form_error('confirm_password',' ', ' '); ?>
          <?php echo form_error('recaptcha_response_field',' ', ' '); ?>
          <?php echo form_error('email',' ', ' '); ?>
          <?php echo isset($errors['email'])?$errors['email']:''; ?>
          <a href="#" class="close">&times;</a>
        </div>

  		<?php } ?>

  			<div style="font-size:12px; margin:10px 0;">
	Please note that you will need to enter a valid e-mail address before your account is activated.
	You will receive an e-mail at the address you provide that contains an account activation link.
 	</div>

        <button type="submit">Register</button>
  	</form>

 </div>

 <?php echo $recaptcha_html; ?>

 <script>

 	$(document).ready(function(){

		$(window).load(function(){
			$('.register-form').animate({opacity:'1'},1500);
		});

	});
 </script>