<html>

	<head> <h3 style="color:white; margin:40px 30px"><b>Settings</b></h3> </head>

	<body>
        <div style="color:white; margin-left:30px;">

          <p style="font-size:25px;">Change Your Settings? </p>

         <!--Display errors to user-->
          <div style="color:#CC0000">

             <?php if($passDoesntMatch): ?>
		    	New password doesn't match confirmation </br>
		 	 <?php endif; ?> 
          
		  	 <?php if($notValidPass): ?>
		  	 	New password must be at least 6 characters long </br>
		 	 <?php endif; ?>
          
		 	 <?php if($wrongPass): ?>
				Current password is invalid </br>
		  	 <?php endif; ?>

		  	 <?php if($notAvailable): ?>
				This email is already taken
		  	 <?php endif; ?>

          </div>
          <form action=<?=site_url("/account/settings")?>  method="post">

        	<p style="margin-bottom:1px; margin-top:30px">New Password</p>
            <input style="width: 17em; height:2.5em; margin-bottom:30px" type="password" name="newPas">

			<p style="margin-bottom:1px;margin-top:30px">Confirm Password</p>
            <input style="width: 17em; height:2.5em; margin-bottom:30px" type="password" name="passwordConf">

			<p style="margin-bottom:1px;">New mail</p>
          	<input style="width: 17em; height:2.5em; margin-bottom:30px" type="email" name="email">

			<p style="margin-bottom:1px; margin-top:30px"><b>Current Password</b> <span style="font-size:14px">(used to confirm your changes)</span></p>
          	<input style="width: 17em; height:2.5em; margin-bottom:30px" type="password" name="currentPas" required>

			<button type="submit" class="button small">Update</button>

          </form>
        </div>

	</body>
</html>